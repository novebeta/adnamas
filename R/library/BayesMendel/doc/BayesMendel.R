###################################################
### chunk number 1: options
###################################################
options(width=65)


###################################################
### chunk number 2: package
###################################################
library(BayesMendel)


###################################################
### chunk number 3: package
###################################################

# Load required BRCAPRO penetrance and competing risks objects
data(BRCApenet.metaDSL.2008, death.othercauses, CBRCApenet.metaDSL.2009)
data(compriskSurv)

# Change the output for future risk to be calculated in intervals of 2 years instead of the default of 5 years.
# Leave all other parameters as set.
myparams <- brcaparams(age.by=2)

# Load example family
data(brca.fam)

# Run BRCAPRO with family history information
brcapro(family=brca.fam)



###################################################
### chunk number 4: package
###################################################
# An Ashkenazi Jewish family
brcapro(family=brca.fam, allef.type="AJ")

# Input your own prevalence estimates
myparams <- brcaparams(allef=c(0.005, 0.0055))
brcapro(family=brca.fam, allef.type="other", params=myparams)


###################################################
### chunk number 5: package
###################################################
data(BRCApenet.Italian.2008)
myparams <- brcaparams(penetrance = BRCApenet.Italian.2008)
brcapro(family=brca.fam, allef.type="nonAJ", params=myparams)


###################################################
### chunk number 6: package
###################################################
data(BRCAbaseline.race.2008)  # First need to load the baseline values
brcapro(family=brca.fam, race="Hispanic")


###################################################
### chunk number 7: package
###################################################
# Add the testing results for BRCA1 and BRCA2
BRCA1 <- BRCA2 <- TestOrder <- rep(0,nrow(brca.fam))
germline.testing <- data.frame(BRCA1,BRCA2,TestOrder)
germline.testing[2,] <- c(2,0,1)
brcapro(family=brca.fam, germline.testing=germline.testing)


###################################################
### chunk number 8: package
###################################################
# Add the testing results for breast cancer markers
marker.testing <- data.frame(matrix(rep(0,nrow(brca.fam)*4),ncol=4))
colnames(marker.testing) <- c("ER","CK14","CK5.6","PR")
brca.fam[1,"AffectedBreast"] <- 1 
marker.testing[1,"ER"] <- 2 
brcapro(family=brca.fam, germline.testing=germline.testing, marker.testing=marker.testing)


###################################################
### chunk number 9: package
###################################################
# Add the information for oophorectomy
Oophorectomy <- c(1,rep(0,(nrow(brca.fam)-1)))
AgeOophorectomy <- c(30,rep(1,(nrow(brca.fam)-1)))
oophorectomy <- data.frame(Oophorectomy,AgeOophorectomy)
brcapro(family=brca.fam, germline.testing=germline.testing, marker.testing=marker.testing, oophorectomy=oophorectomy)


###################################################
### chunk number 10: package
###################################################

# Load required MMRpro penetrance and competing risks object
data(MMRpenet.2008, death.othercauses)

# Load example family
data(MMR.fam)

# Change the output for future risk to be calculated up to age 95 instead of the default 85.
# Leave all other parameters as set.
myparams <- MMRparams(age.to=95)

# Run BRCAPRO with family history information
MMRpro(family=MMR.fam, params=myparams)



###################################################
### chunk number 11: package
###################################################

## The counselee's father was tested for germline mutations in MLH1 and MSH2, but none were found.
## No testing for MSH6 was done.
MLH1 <- MSH2 <- MSH6 <- TestOrder <- rep(0, nrow(MMR.fam))
germline.testing = data.frame(MLH1, MSH2, MSH6, TestOrder)
germline.testing[3,] <- c(2,2,0,1)  

MMRpro(family=MMR.fam, germline.testing = germline.testing)



###################################################
### chunk number 12: package
###################################################

## Now let's say the counselee's sister has a colorectal tumor

MMR.fam[7, "AffectedColon"] <- 1

## The counselee's sister's tumor was found to be MSI high.
## Add in this MSI result.

MSI <- location <- rep(0, nrow(MMR.fam))
marker.testing <- data.frame(MSI, location)
marker.testing[7, "MSI"] <- 1

MMRpro(family = MMR.fam, marker.testing = marker.testing)



###################################################
### chunk number 13: package
###################################################

# Load required PancPRO penetrance and competing risks object
data(pancpenet.2008, death.othercauses)

# Change the output for future risk to be calculated in age intervals of 1 year up to age 65 instead of the default 5 years.
# Leave all other parameters as set.
myparams <- pancparams(age.by=1, age.to=65)

# Load example family
data(panc.fam)

# Run PancPRO with family history information
pancpro(family=panc.fam, params=myparams)



###################################################
### chunk number 14: package
###################################################

# Load a MelaPRO penetrance and competing risks object
data(melapenet.HBI.2009, death.othercauses)

# Load example family
data(mela.fam)

# Change likelihood ratio for single melanomas among noncarriers from default 0.702 to 0.80
myparams <- melaparams(spm.lr.noncarrier=0.80)

# Run PancPRO with family history information
melapro(family=mela.fam, params=myparams)



###################################################
### chunk number 15: package
###################################################

## The counselee's sister was tested for germline mutations in P16, and one was found.
## Proband was also tested, but no mutation was found.
P16 <- TestOrder <- rep(0, nrow(mela.fam))
germline.testing = data.frame(P16, TestOrder)
germline.testing[4,] <- c(1,1)
germline.testing[1,] <- c(2,2)

melapro(family=mela.fam, germline.testing = germline.testing)



###################################################
### chunk number 16: package
###################################################
#pdf("brcafamplot.pdf")
status <- c(0,0,1,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0)
brca.fam <- data.frame(brca.fam, status)
myfamily <- new("BayesMendel", family=brca.fam, counselee.id=1)
plot(myfamily)
#dev.off()


###################################################
### chunk number 17: package
###################################################

#pdf("mmrfamplot.pdf")
mmrpro.out <- MMRpro(family=MMR.fam, counselee.id=1)
plot(mmrpro.out)
#dev.off()


