= svSocket News

== Changes in svSocket 0.9-51

* processSocket() no longer adds en empty line at the top of R commands (bug
  corrected).


== Changes in svSocket 0.9-50

* processSocket() now calls parseText() from svMisc >= 0.9-60 instead of Parse().

* When Echo is TRUE and we are not in hidden mode, results are echoed directly
  in the R console as they are available, and not any more at the end of the
  calculation.
  
* A new type of connection is added: a 'sockclientconn' that allows to redirect
  output (append or write-only, for the moment) to a SciViews socket client. It
  is created by using socketClientConnection() and has a specific summary()
  method. It inherits from a 'sockconn' object and should behave similarly.
  
* parSocket() has a new argument, clientsocket, that allows to pass the Tcl name
  of the client's socket. This is required to use socketClientConnection() by
  providing only the client's name (and thus, the required Tcl socket name is
  obtained through the property parSocket(....)$clientsocket, if it was
  previously recorded). The default process function, processSocket() is changed
  to record the Tcl socket in parSocket() each time a client connects to the
  server and sends its first command through it.


== Changes in svSocket 0.9-49

* Small change in startSocketServer(): the Tcl/Tk callback function now calls
  a closure located in TempEnv (SocketServerProc).


== Changes in svSocket 0.9-48

* svTaskCallbackManager() added to allow callbacks to be executed after each
  (complete) R code send by a client to the server, as well as, any top-level
  task run at the R console.


== Changes in svSocket 0.9-47

* The server now calls taskCallbacks on non-hidden mode after code evaluation


== Changes in svSocket 0.9-46

* evalServer() slightly reworked

* sendSocketServer() eliminated (superseeded by evalServer())


== Changes in svSocket 0.9-45

* Bug correction in evalServer()


== Changes in svSocket 0.9-44

* Added function evalServer() for R interprocess communication using this R
  socket server mechanism


== Changes in svSocket 0.9-43

* Example added in processSocket(), implementing a simple REPL

* A new function, sendSocketServer() is added to send and evaluate commands from
  one R instance (client) to another one (a R socket server)


== Changes in svSocket 0.9-43

* Polishing package for CRAN submission


== Changes in svSocket 0.9-42

* Made compatible with R 2.6.x (previous package was R >= 2.7.0).


== Changes in svSocket 0.9-41

* Correction in startSocketServer(): the SocketServerProc function was not
  protected against garbage collection. Consequently, the socket server stopped
  working at unpredictable events.

* Correction of a bug preventing processSocket() to display error messages.
  Instead, I got:
    Error in ngettext(1, "Error: ", domain = "R") :
      argument "msg2" is missing, with no default


== Changes in svSocket 0.9-40

This is the first version distributed on R-forge. It is completely refactored
from older versions (on CRAN since 2003) to make it run with SciViews-K and
Komodo Edit (Tinn-R is also supported, but not SciViews-R Console any more).
