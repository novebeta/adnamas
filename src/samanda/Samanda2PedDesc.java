package samanda;

import com.nexes.wizards.Wizard;
import com.nexes.wizards.WizardPanelDescriptor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import pedigree.Pelican2;
import pedigree.PelicanPerson;
import samanda.properties.LanguagesController;

public class Samanda2PedDesc extends WizardPanelDescriptor {
    public static final String IDENTIFIER = "SAMANDA2_PEDIGREE_PANEL";
    Pelican2 fami;
    public LanguagesController lc;
    public Samanda2PedDesc(LanguagesController l) // TODO Auto-generated constructor stub
    {
//        this.getWizard().add(new JMenuBar());
        lc = l;
        System.out.println("Create PedDesc");
        Pelican2.parentPane = Wizard.parent;
        fami = new Pelican2(lc);
//        fami.createMenuBar();
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(new JScrollPane(fami));
    }
    @Override
    public Object getNextPanelDescriptor() {
        return SamandaResultDesc.IDENTIFIER;
    }
    public Object getBackPanelDescriptor() {
        return RekamMedikDesc.IDENTIFIER;
    }
    @Override
    public boolean isAllowMoveForward() {
        try {
            //if(fami)
            getWizard().famPelican = fami;
//            Wizard.Samanda_file = fami.getFilename();
            Wizard.Samanda_file = fami.getMatrix();
            Wizard.proband = fami.probandId;
            Wizard.Samanda_image = fami.getPedImage();
            if (Wizard.Samanda_file.isEmpty() || Wizard.proband.isEmpty()) {
                //getWizard().setCurrentPanel(SamandaPedDesc.IDENTIFIER);
                return false;
            }
            //return;
            PelicanPerson pp = fami.getPelicanPersonByID(fami.probandId);
            getWizard().setMenarche(pp.menarche);
            getWizard().setParity(pp.parity);
            getWizard().setAgefirst(pp.agefirst);
            getWizard().setHeight(pp.height);
            getWizard().setWeight(pp.weight);
            getWizard().setMens_cycle(pp.menscycle);
            getWizard().setFPmethod(pp.birthcontroltype);
            getWizard().setHorm_t(pp.HRT);
            getWizard().setSmooking(pp.smooking);
            getWizard().setMenop(pp.menopause);
            getWizard().setLactation(pp.laktasi);
            String[] result = null;
            if (getWizard().getWeight() == 0 && getWizard().getHeight() == 0 && getWizard().getParity() == 0
                    && getWizard().getAgefirst() == 0 && getWizard().getMenarche() == 0 && getWizard().getMens_cycle() == 0
                    && getWizard().getFPmethod() == 0 && getWizard().getSmooking() == 0 && getWizard().getHorm_t() == 0
                    && getWizard().getMenop() == 0) {
                JOptionPane.showMessageDialog(getWizard().getParent(), "Please, validate proband's data.");
                return false;
            }
//            if (Utils.FileOrDirectoryExists(fami.getCustDir())) {
//                Utils.DeleteFile(fami.getCustDir());
//            }
//            Utils.InsertDataORM(Utils.DATA_DIR + "History/" + getWizard().customers.Directory + "/pedigree.sam", "0", fami);
            getWizard().setNextFinishButtonEnabled(true);
            getWizard().setBackButtonEnabled(true);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(Samanda2PedDesc.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    @Override
    public void aboutToDisplayPanel() {
//        if (!Utils.FileOrDirectoryExists(Utils.DATA_DIR + "History")) {
//            Utils.CreateDirectory(Utils.DATA_DIR + "History");
//        }
//        if (!Utils.FileOrDirectoryExists(Utils.DATA_DIR + "History/" + getWizard().customers.Directory)) {
//            Utils.CreateDirectory(Utils.DATA_DIR + "History/" + getWizard().customers.Directory);
//        }
//        fami.setCustDir(Utils.DATA_DIR + "History/" + getWizard().customers.Directory + "/pedigree.sam");
//        if (Utils.FileOrDirectoryExists(Utils.DATA_DIR + "History/" + getWizard().customers.Directory + "/pedigree.sam")) {
//            fami.openFileSamanda2(Utils.DATA_DIR + "History/" + getWizard().customers.Directory + "/pedigree.sam", "0");
//            fami.openSamanda2(getWizard().customers);
//        } else {
//            fami.newPedigree();
//        }
        getWizard().setToolBar(fami.createToolBar());
        fami.openSamanda2(getWizard().customers);
    }
    @Override
    public void aboutToHidePanel() {
        getWizard().delToolBar();
    }
    private void runCode() {
        Thread t = new Thread() {
            public void run() {
                try {
                } catch (Exception e) {
                    System.out.println(e.toString());
                }
            }
        };
        t.start();
    }
}
