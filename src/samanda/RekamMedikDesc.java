/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samanda;

import com.nexes.wizards.WizardPanelDescriptor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import samanda.properties.LanguagesController;

/**
 *
 * @author axioo
 */
public class RekamMedikDesc extends WizardPanelDescriptor implements ActionListener {
    public static final String IDENTIFIER = "REKAMMEDIK_PANEL";
    RekamMedik rekamMedik;
    public LanguagesController lc;
    public RekamMedikDesc(LanguagesController l) {
        lc = l;
        //System.out.println("Create Agrement Desc");
        rekamMedik = new RekamMedik(lc);
        //
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(rekamMedik);
        //rekamMedik.addCheckBoxActionListener(this);
    }
    @Override
    public Object getNextPanelDescriptor() {
//        if (getWizard().getTitle().equals("Samanda 2")) {
//            return Samanda2PedDesc.IDENTIFIER;
//        } else {
//            return NonGeneticDesc.IDENTIFIER;
//        }
        return Samanda2PedDesc.IDENTIFIER;
    }
    @Override
    public boolean isAllowMoveForward() {
        getWizard().customers = rekamMedik.getCustomers();
        if (getWizard().customers == null) {
            JOptionPane.showMessageDialog(getWizard().getParent(), "Please select, before continue");
            return false;
        } else if (getWizard().customers.Birth_Date == null) {
            return false;
        }
        return true;
    }
    @Override
    public Object getBackPanelDescriptor() {
        return AgreementDesc.IDENTIFIER;
    }
    @Override
    public void aboutToDisplayPanel() {
        rekamMedik.setParentPane(getWizard().getParent());
    }
    @Override
    public void aboutToHidePanel() {
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
