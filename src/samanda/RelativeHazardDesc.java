package samanda;

import com.nexes.wizards.Wizard;
import com.nexes.wizards.WizardPanelDescriptor;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Properties;
import pedigree.Utils;

public class RelativeHazardDesc extends WizardPanelDescriptor {
    public static final String IDENTIFIER = "RELATIVEHAZARD_PANEL";
    RelativeHazard rHazard;
    //File directory = new File(".");
    public RelativeHazardDesc() {
        rHazard = new RelativeHazard();
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(rHazard);
    }
    public Object getNextPanelDescriptor() {
        return SamandaPedDesc.IDENTIFIER;
    }
    public Object getBackPanelDescriptor() {
        return NonGeneticDesc.IDENTIFIER;
    }
    public void aboutToDisplayPanel() {
        rHazard.setProgressValue(0);
        //rHazard.addTxtClaus("Please wait...");
        getWizard().setNextFinishButtonEnabled(false);
        getWizard().setBackButtonEnabled(false);
        runCode();
    }
    private void runCode() {
        Thread t = new Thread() {
            public void run() {
                try {
                    rHazard.setProgressValue(0);
                    File directory = new File(".");
                    String currentPath = directory.getCanonicalPath().replace("\\", "/");
                    Properties prop = new Properties();
                    try {
                        //load a properties file
                        prop.load(new FileInputStream(Utils.DATA_DIR + "param.properties"));
                        Enumeration em = prop.keys();
                        while (em.hasMoreElements()) {
                            String str = (String) em.nextElement();
                            //Wizard.s.eval(str + "=" + prop.get(str), false);
                            Wizard.code.addRCode(str + "=" + prop.get(str));
                        }
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    Wizard.code.addRCode("pathLib = '" + currentPath + "/data'");
                    //Wizard.s.eval("pathLib = '" + currentPath + "/data'", false);
                    StringBuilder dataCode = Utils.ReadFile("data", "data.jev");
                    Wizard.code.addRCode(dataCode.toString());
                    //REXP res = getWizard().s.eval("ols <- R.version.string");
                    rHazard.setProgressValue(5);
                    //String[] result = null;
                    rHazard.setProgressValue(10);
                    //result = res.asStrings();
                    rHazard.setProgressValue(15);
                    //rHazard.addTxtClaus(result[0]);
                    rHazard.setProgressValue(25);
                    //rHazard.addTxtClaus("Calculate Hazard Relative...");
                    rHazard.setProgressValue(30);
                    Thread.sleep(100);
//                    rHazard.addTxtClaus("Transmitting Data...");
                    rHazard.setProgressValue(40);
                    //StringUtils.join
                    Wizard.code.addRCode("hr <- RelativeHazard("
                            + getWizard().getWeight() + ","
                            + getWizard().getHeight() + ","
                            + getWizard().getParity() + ","
                            + getWizard().getAgefirst() + ","
                            + getWizard().getMenarche() + ","
                            + getWizard().getMens_cycle() + ","
                            + getWizard().getFPmethod() + ","
                            + getWizard().getSmooking() + ","
                            + getWizard().getHorm_t() + ","
                            + getWizard().getMenop()
                            + ")");
//                    rHazard.addTxtClaus("Data Successfully Transmitted");
                    Wizard.caller.setRCode(Wizard.code);
                    Wizard.caller.runAndReturnResultOnline("ols");
                    String[] result = Wizard.caller.getParser().getAsStringArray("ols");
                    rHazard.model.getDataVector().removeAllElements();
                    rHazard.model.insertRow(rHazard.table.getRowCount(), new Object[]{"Weight", getWizard().getWeight()});
                    rHazard.model.insertRow(rHazard.table.getRowCount(), new Object[]{"Height", getWizard().getHeight()});
                    rHazard.model.insertRow(rHazard.table.getRowCount(), new Object[]{"Parity", getWizard().getParity()});
                    rHazard.model.insertRow(rHazard.table.getRowCount(), new Object[]{"Age at First Live-Born Child", getWizard().getAgefirst()});
                    rHazard.model.insertRow(rHazard.table.getRowCount(), new Object[]{"Menarche", getWizard().getMenarche()});
                    rHazard.model.insertRow(rHazard.table.getRowCount(), new Object[]{"Mens Cycle", getWizard().getMens_cycle()});
                    rHazard.model.insertRow(rHazard.table.getRowCount(), new Object[]{"Birth Control Method", getWizard().getFPmethod()});
                    rHazard.model.insertRow(rHazard.table.getRowCount(), new Object[]{"Smoking", getWizard().getSmooking()});
                    rHazard.model.insertRow(rHazard.table.getRowCount(), new Object[]{"HRT", getWizard().getHorm_t()});
                    rHazard.model.insertRow(rHazard.table.getRowCount(), new Object[]{"Age at Menopause", getWizard().getMenop()});
                    rHazard.setProgressValue(55);
                    //result = res.asStrings();
                    Wizard.caller.runAndReturnResultOnline("hr");
                    double[] hr = Wizard.caller.getParser().getAsDoubleArray("hr");
                    double result_hr = hr[0];
                    //System.out.println(result_hr);
                    rHazard.setProgressValue(65);
                    Thread.sleep(100);
                    rHazard.setProgressValue(100);
//                    rHazard.addTxtClaus("Done.");
                    DecimalFormat forD = new DecimalFormat("#.###");
                    getWizard().setHr(Double.valueOf(forD.format(result_hr)));
                    rHazard.setResult("" + getWizard().getHr());
                    getWizard().setNextFinishButtonEnabled(true);
                    getWizard().setBackButtonEnabled(true);
                } catch (InterruptedException e) {
                    rHazard.setProgressValue(0);
//                    rHazard.addTxtClaus("An Error Has Occurred");
                    getWizard().setBackButtonEnabled(true);
                } catch (Exception e) {
                    System.out.println(e.toString());
                }
            }
        };
        t.start();
    }
}
