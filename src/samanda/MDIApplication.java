/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samanda;

import com.j256.ormlite.logger.LocalLog;
import com.tomtessier.scrollabledesktop.JScrollableDesktopPane;
import de.muntjak.tinylookandfeel.Theme;
import de.muntjak.tinylookandfeel.ThemeDescription;
import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.QUESTION_MESSAGE;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import pedigree.Utils;
import samanda.properties.LanguagesController;
import samanda.zaval.tools.i18n.translator.BundleManager;
import samanda.zaval.tools.i18n.translator.LangItem;

public class MDIApplication extends javax.swing.JFrame {
    //public static Rsession session;
    private JScrollableDesktopPane desktopPane;
    public static Process p;
    private Runtime run;
    public Thread t;
    LanguagesController langController_nl;
    public Locale useLocale;
    //private Samanda s;
    /**
     * Creates new form MDIApplication
     */
    class Item {
        private String id;
        private String description;
        public Item(String id, String description) {
            this.id = id;
            this.description = description;
        }
        public String getId() {
            return id;
        }
        public String getDescription() {
            return description;
        }
        public String toString() {
            return description;
        }
    }
    class ItemRenderer extends BasicComboBoxRenderer {
        public Component getListCellRendererComponent(
                JList list, Object value, int index,
                boolean isSelected, boolean cellHasFocus) {
            super.getListCellRendererComponent(list, value, index,
                    isSelected, cellHasFocus);
            if (value != null) {
                Item item = (Item) value;
                setText(item.getDescription().toUpperCase());
            }
            if (index == -1) {
                Item item = (Item) value;
                setText("" + item.getId());
            }
            return this;
        }
    }
    public MDIApplication() {
        try {
            System.setProperty(LocalLog.LOCAL_LOG_LEVEL_PROPERTY, "FATAL");
//            File file = new File(MDIApplication.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath() + "/lang/language.properties");
            String path = Utils.getJarContainingFolder(MDIApplication.class) + File.separator + "lang" + File.separator + "language.properties";
            System.out.println(path);
            BundleManager bundle = new BundleManager(path);
            Vector model = new Vector();
            for (int i = 0; i < bundle.getBundle().getLangCount(); ++i) {
                LangItem lang2 = bundle.getBundle().getLanguage(i);
                model.addElement(new Item(lang2.getLangId(), lang2.getLangDescription()));
            }
            JFrame frame = new JFrame("Languange");
            JComboBox cbox = new JComboBox(model);
////            cbox.setRenderer(new ItemRenderer());
            JOptionPane.showMessageDialog(frame, cbox, "Select Language", QUESTION_MESSAGE);
            Item c = (Item) cbox.getSelectedItem();
            String[] parts = c.id.split("_");
            if (parts.length < 2) {
                useLocale = Locale.getDefault();
            } else {
                useLocale = new Locale(parts[0], parts[1]);
            }
            langController_nl = new LanguagesController(useLocale);
            Samanda2 s = new Samanda2("Samanda", langController_nl);
//            desktopPane.add(s.wizard.getDialog());
            //setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            //runCode();
            // StartRServe();
            //Thread.sleep(2000);
//            RserverConf con = new RserverConf("localhost", 6311, null, null, null);
//            session = Rsession.newInstanceTry(System.out, con);
//            session.rmAll();
            //Wizard.s = session;
//            initComponents();
//            desktopPane = new JScrollableDesktopPane();
//            this.setExtendedState(MAXIMIZED_BOTH);
//            desktopPane.putClientProperty("JDesktopPane.dragMode", "outline");
//            desktopPane.registerMenuBar(menuBar);
//            setContentPane(desktopPane);
//            menuBar.add(helpMenu);
        } catch (Exception ex) {
            Logger.getLogger(MDIApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void runCode() {
        t = new Thread() {
            @Override
            public void run() {
                try {
                    StartRServe();
                } catch (InterruptedException e) {
                    System.out.println(e.toString());
                } catch (Exception e) {
                    System.out.println(e.toString());
                }
            }
        };
        t.start();
    }
    private void StartRServe() throws InterruptedException {
        try {
            //ProcessBuilder pb = new ProcessBuilder("run.bat");
            //p = run.exec("cmd /c TASKKILL      /IM Rserve.exe /f /t");
            p = run.exec("cmd /c run.bat");
            //p = pb.start();
            //p.waitFor();
        } catch (IOException ex) {
            Logger.getLogger(MDIApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        exitMenuItem = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        mnClausWiz = new javax.swing.JMenuItem();
        mnSamanda2Wizd = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        contentMenuItem = new javax.swing.JMenuItem();
        aboutMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Samanda");
        setExtendedState(MAXIMIZED_BOTH);

        fileMenu.setText(langController_nl.getWord("general.file"));

        jMenuItem2.setText(langController_nl.getWord("general.option"));
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        fileMenu.add(jMenuItem2);
        fileMenu.add(jSeparator1);

        exitMenuItem.setText(langController_nl.getWord("general.exitSamanda"));
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        jMenu1.setText(langController_nl.getWord("general.run"));

        jMenuItem1.setText("Samanda Wizard");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        mnClausWiz.setText("Claus Wizard");
        mnClausWiz.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnClausWizActionPerformed(evt);
            }
        });
        jMenu1.add(mnClausWiz);

        mnSamanda2Wizd.setText(langController_nl.getWord("general.samandaWizard"));
        mnSamanda2Wizd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnSamanda2WizdActionPerformed(evt);
            }
        });
        jMenu1.add(mnSamanda2Wizd);

        menuBar.add(jMenu1);

        helpMenu.setMnemonic('h');
        helpMenu.setText(langController_nl.getWord("general.help"));

        contentMenuItem.setMnemonic('c');
        contentMenuItem.setText(langController_nl.getWord("general.content"));
        helpMenu.add(contentMenuItem);

        aboutMenuItem.setMnemonic('a');
        aboutMenuItem.setText(langController_nl.getWord("general.about"));
        helpMenu.add(aboutMenuItem);

        menuBar.add(helpMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 281, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        Samanda s = new Samanda(desktopPane, "Samanda");
        desktopPane.add(s.wizard.getDialog());
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here: menu settings
        //JOptionPane.showMessageDialog(desktopPane, "test");
        JInternalFrame set = new Settings(desktopPane);
        desktopPane.add(set);
        //set.setSelected(true);
        set.setVisible(true);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void mnClausWizActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnClausWizActionPerformed
        // TODO add your handling code here:
        ClausWiz claus = new ClausWiz(desktopPane, "Claus", langController_nl);
        desktopPane.add(claus.wizard.getDialog());
        //set.setSelected(true);

    }//GEN-LAST:event_mnClausWizActionPerformed
    public static void exitprocedure() {
//        p.destroy();
//        try {
//             p = run.exec("cmd /c TASKKILL      /IM Rserve.exe /f /t");
//
//        } catch (IOException ex) {
//            Logger.getLogger(MDIApplication.class.getName()).log(Level.SEVERE, null, ex);
//        }
        System.exit(0);
    }

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        exitprocedure();

    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void mnSamanda2WizdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnSamanda2WizdActionPerformed
        Samanda2 s = new Samanda2("Samanda", langController_nl);
        desktopPane.add(s.wizard.getDialog());
    }//GEN-LAST:event_mnSamanda2WizdActionPerformed
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        Locale.setDefault(new Locale("en", "US"));
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            javax.swing.UIManager.setLookAndFeel("de.muntjak.tinylookandfeel.TinyLookAndFeel");
//            Theme.getAvailableThemes();
            for (ThemeDescription info : Theme.getAvailableThemes()) {
                if ("YQ Theme".equals(info.getName())) {
                    Theme.loadTheme(info);
                    break;
                }
            }
//            javax.swing.UIManager.setLookAndFeel(new TinyLookAndFeel());
//        javax.swing.UIManager.getLookAndFeelDefaults()
//                .put("defaultFont", new Font("Arial", Font.PLAIN, 12));
//            javax.swing.UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                System.out.println(info.getName());
//                System.out.println(info.getClassName());
//                if ("Metal".equals(info.getName())) {
//                    //javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                   // break;
//                }
//
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(MDIApplication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(MDIApplication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(MDIApplication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MDIApplication.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MDIApplication.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(MDIApplication.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(MDIApplication.class.getName()).log(Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Locale.setDefault(new Locale("en", "US"));
                MDIApplication mdi = new MDIApplication();
//                mdi.jMenuItem1.setVisible(false);
//                mdi.mnClausWiz.setVisible(false);
                mdi.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//                mdi.addWindowListener(new WindowAdapter() {
//                    @Override
//                    public void windowClosing(WindowEvent e) {
//                        exitprocedure();
//                    }
//                });
//                mdi.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private javax.swing.JMenuItem contentMenuItem;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem mnClausWiz;
    private javax.swing.JMenuItem mnSamanda2Wizd;
    // End of variables declaration//GEN-END:variables
}
