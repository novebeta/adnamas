/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samanda;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.labels.StandardCategorySeriesLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import pedigree.Utils;
import samanda.properties.LanguagesController;

/**
 *
 * @author axioo
 */
public class SamandaResult extends javax.swing.JPanel {
    private final String[] colNames = {"Age", "Genetic", "Non Genetic", "Combine", "Claus"};
//    private final String[] colNames = {"Age", "Genetic", "Total Risk", "Claus"};
    DefaultTableModel model = new DefaultTableModel(new Object[][][][]{},
            colNames);
    JFreeChart jfreechart;
    BufferedImage img;
    private double hrD;
    private DefaultCategoryDataset dataset = new DefaultCategoryDataset();
    private ArrayList<Integer> tAge = new ArrayList<>();
    private ArrayList<Double> tGenetic = new ArrayList<>();
    private ArrayList<Double> tNonGenetic = new ArrayList<>();
    private ArrayList<Double> tTotalRisk = new ArrayList<>();
    private ArrayList<Double> tClaus = new ArrayList<>();
    public LanguagesController lc;
    /**
     * Creates new form SamandaResult
     */
    public SamandaResult(LanguagesController l) {
        lc = l;
        initComponents();
        table.getColumn("Age").setPreferredWidth(25);
    }
    public void setTableAndDataset(int age, double genetic, double non, double total, double claus) {
        DecimalFormat twoDForm = new DecimalFormat("#.###");
        model.insertRow(table.getRowCount(), new Object[]{age,
            Double.valueOf(twoDForm.format(genetic)),
            Double.valueOf(twoDForm.format(non)),
            Double.valueOf(twoDForm.format(total)),
            Double.valueOf(twoDForm.format(claus))
        });
        //double a2 = Double.valueOf(twoDForm.format(genetic));
        dataset.addValue(Double.valueOf(twoDForm.format(genetic)),
                "Genetic Risk", "" + age);
        dataset.addValue(Double.valueOf(twoDForm.format(non)),
                "Non Genetic Risk", "" + age);
        dataset.addValue(Double.valueOf(twoDForm.format(total)),
                "Combine Risk", "" + age);
        dataset.addValue(Double.valueOf(twoDForm.format(claus)),
                "Claus", "" + age);
        tAge.add(age);
        tGenetic.add(Double.valueOf(Utils.forD.format(genetic)));
        tNonGenetic.add(Double.valueOf(Utils.forD.format(genetic)));
        tTotalRisk.add(Double.valueOf(Utils.forD.format(total)));
        tClaus.add(Double.valueOf(Utils.forD.format(claus)));
    }
    public void clearTable() {
        model.getDataVector().removeAllElements();
        dataset.clear();
    }
    public void setPlot(String f) {
//        JPanel panel = new JPanel() {
//
//            @Override
//            protected void paintComponent(Graphics g) {
//                super.paintComponent(g);
//                BufferedImage s;
//                //Image image;
//                try {
//                    s = ImageIO.read(new File("data/plot.png"));
//                    Graphics2D g2 = (Graphics2D)g;
//                    g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
////
//                    g2.drawImage(s, 0, 0, getWidth(), getHeight(), this);
//                } catch (Exception ex) {
//                    Logger.getLogger(SamandaResult.class.getName()).log(Level.SEVERE, null, ex);
//                    return;
//                }
//
//            }
//
//        };
        try {
            //ImagePanel panel1 = new ImagePanel(ImageIO.read(new File("data/plot.png")));
//            ImagePanel panel1 = new ImagePanel();
//            panel1.addNewPhoto(ImageIO.read(f));
//            panel1.repaint();
            InputStream is = new BufferedInputStream(new FileInputStream(f));
            ShowCanvas panel1 = new ShowCanvas(ImageIO.read(is));
            //panel1.setPreferredSize(768,768);
            //MyCanvas panCanvas = new MyCanvas(ImageIO.read(f));
            //ImageZoomerFrame panel1 = new ImageZoomerFrame(ImageIO.read(f),10);
            //panel1.setPreferredSize(d);
//            JPanel panel = new JPanel();
            JScrollPane sc = new JScrollPane();
            sc.setAutoscrolls(true);
            //sc.add(panel1);
            sc.setViewportView(panel1);
//            panel.add(panCanvas);
//
            tabbedPane.addTab(lc.getWord("general.plot"), sc);
            //panel1.repaint();
        } catch (IOException ex) {
            Logger.getLogger(SamandaResult.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void RemoveallTab() {
        tabbedPane.removeAll();
    }
    public void setChart() {
        tabbedPane.addTab(lc.getWord("general.chart"), new JScrollPane(createDemoPanel()));
    }
    public void SetReportPdf() {
        PdfReport rpt = new PdfReport(lc);
        rpt.SetDataTable(tAge, tGenetic, tNonGenetic, tTotalRisk);
        rpt.setImageChart(img);
        rpt.SetHr(getHrD());
        tabbedPane.addTab(lc.getWord("general.reportPDF"), new JScrollPane(rpt));
    }
    public JFreeChart getChart() {
        return jfreechart;
    }
    public void setSubtitle(double hr) {
        double result = Double.valueOf(Utils.forD.format((hr - 1) * 100));
        if (hr >= 2) {
            jfreechart.addSubtitle(new TextTitle("Relative Environment Hazard " + Double.valueOf(Utils.forD.format((hr)))));
        } else {
            jfreechart.addSubtitle(new TextTitle("Excess Environment Risk " + result + " %"));
        }
        setHrD(hr);
    }
    public void setSubtitle(ArrayList<String> subTitle) {
        for (String st : subTitle) {
            jfreechart.addSubtitle(new TextTitle(st));
        }
    }
    public void setSubtitleClaus(double claus) {
        jfreechart.addSubtitle(new TextTitle("Claus " + claus));
        //hrD = hr;
    }
    private JFreeChart createChart() {
        jfreechart = ChartFactory.createBarChart("Samanda Calculation", "Age", "Risk Value", dataset, PlotOrientation.VERTICAL, true, true, false);
        //jfreechart.addSubtitle(new TextTitle("Hazard Relative " + 1.0));
        CategoryPlot categoryplot = (CategoryPlot) jfreechart.getPlot();
        categoryplot.setDomainGridlinesVisible(true);
        categoryplot.setRangeCrosshairVisible(true);
        categoryplot.setRangeCrosshairPaint(Color.blue);
        categoryplot.setBackgroundPaint(Color.WHITE);
        //NumberAxis numberaxis = (NumberAxis)categoryplot.getRangeAxis();
        //numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        BarRenderer barrenderer = (BarRenderer) categoryplot.getRenderer();
        barrenderer.setDrawBarOutline(false);
        GradientPaint gradientpaint = new GradientPaint(0.0F, 0.0F, Color.RED, 0.0F, 0.0F, new Color(0, 0, 64));
        GradientPaint gradientpaint1 = new GradientPaint(0.0F, 0.0F, Color.BLUE, 0.0F, 0.0F, new Color(0, 64, 0));
        GradientPaint gradientpaint2 = new GradientPaint(0.0F, 0.0F, Color.MAGENTA, 0.0F, 0.0F, new Color(64, 0, 0));
        GradientPaint gradientpaint3 = new GradientPaint(0.0F, 0.0F, Color.GREEN, 0.0F, 0.0F, new Color(64, 0, 0));
        barrenderer.setSeriesPaint(0, gradientpaint);
        barrenderer.setSeriesPaint(1, gradientpaint1);
        barrenderer.setSeriesPaint(2, gradientpaint2);
        barrenderer.setSeriesPaint(3, gradientpaint3);
        barrenderer.setLegendItemToolTipGenerator(new StandardCategorySeriesLabelGenerator("Tooltip: {0}"));
        CategoryAxis categoryaxis = categoryplot.getDomainAxis();
        categoryaxis.setCategoryLabelPositions(CategoryLabelPositions.createUpRotationLabelPositions(0.52359877559829882D));
        categoryaxis.getLabelFont().deriveFont(8);
        return jfreechart;
    }
    public JPanel createDemoPanel() {
        JFreeChart jfreechart = createChart();
        //img = jfreechart.createBufferedImage(800, 600);
        img = draw(jfreechart, 800, 600);
        return new ChartPanel(jfreechart);
    }
    protected BufferedImage draw(JFreeChart chart, int width, int height) {
        BufferedImage img1
                = new BufferedImage(width, height,
                        BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = img1.createGraphics();
        chart.draw(g2, new Rectangle2D.Double(0, 0, width, height));
        g2.dispose();
        return img1;
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblAgreement = new javax.swing.JLabel();
        jSplitPane1 = new javax.swing.JSplitPane();
        tabbedPane = new javax.swing.JTabbedPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable(model);

        lblAgreement.setBackground(new java.awt.Color(128, 128, 128));
        lblAgreement.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblAgreement.setText("     Result Samanda");
        lblAgreement.setOpaque(true);

        jSplitPane1.setBorder(null);
        jSplitPane1.setRightComponent(tabbedPane);

        jScrollPane1.setPreferredSize(new java.awt.Dimension(300, 100));

        table.setModel(model);
        table.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(table);

        jSplitPane1.setLeftComponent(jScrollPane1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jSplitPane1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(lblAgreement, javax.swing.GroupLayout.DEFAULT_SIZE, 688, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAgreement, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 354, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JLabel lblAgreement;
    private javax.swing.JTabbedPane tabbedPane;
    private javax.swing.JTable table;
    // End of variables declaration//GEN-END:variables
    /**
     * @return the hrD
     */
    public double getHrD() {
        return hrD;
    }
    /**
     * @param hrD the hrD to set
     */
    public void setHrD(double hrD) {
        this.hrD = hrD;
    }
}
