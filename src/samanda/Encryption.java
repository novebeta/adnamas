/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samanda;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import pedigree.Utils;

public class Encryption {

    private static Cipher ecipher;
    private static Cipher dcipher;
    private static String filekey = "data/brca.key";
    // 8-byte initialization vector
    private static byte[] iv = {
        (byte) 0xB2, (byte) 0x12, (byte) 0xD5, (byte) 0xB2,
        (byte) 0x44, (byte) 0x21, (byte) 0xC3, (byte) 0xC3
    };

    public static void GenerateKey() throws NoSuchAlgorithmException, Exception {
        SecretKey key = KeyGenerator.getInstance("DES").generateKey();
        writeToFile(filekey, key);
    }

    public static void setFileKeyPath(String path) {
        filekey = path;
    }

    private static void writeToFile(String filename, Object object) throws Exception {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            fos = new FileOutputStream(new File(filename));
            oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
            oos.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (oos != null) {
                oos.close();
            }
            if (fos != null) {
                fos.close();
            }
        }
    }

    private static Object readFromFile(String filename) throws Exception {
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        Object object = null;

        try {
            fis = new FileInputStream(new File(filename));
            ois = new ObjectInputStream(fis);
            object = ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (ois != null) {
                ois.close();
            }
            if (fis != null) {
                fis.close();
            }
        }
        return object;
    }
//    public static void main(String[] args) {
//
//        try {
//
//
//            AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
//
//            ecipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
//
//            dcipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
//
//            ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
//
//            dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
//
//            encrypt(new FileInputStream("cleartext.txt"), new FileOutputStream("encrypted.dat"));
//
//            decrypt(new FileInputStream("encrypted.dat"), new FileOutputStream("cleartext-reversed.txt"));
//
//        } catch (FileNotFoundException e) {
//            System.out.println("File Not Found:" + e.getMessage());
//            return;
//        } catch (InvalidAlgorithmParameterException e) {
//            System.out.println("Invalid Alogorithm Parameter:" + e.getMessage());
//            return;
//        } catch (NoSuchAlgorithmException e) {
//            System.out.println("No Such Algorithm:" + e.getMessage());
//            return;
//        } catch (NoSuchPaddingException e) {
//            System.out.println("No Such Padding:" + e.getMessage());
//            return;
//        } catch (InvalidKeyException e) {
//            System.out.println("Invalid Key:" + e.getMessage());
//            return;
//        }
//
//    }

    public static void encrypt(InputStream is, OutputStream os) {

        try {

            if (!Utils.FileOrDirectoryExists(filekey)) {
                GenerateKey();
            }
            SecretKey key = (SecretKey) readFromFile(filekey);
            AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
            ecipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
            byte[] buf = new byte[1024];

// bytes at this stream are first encoded

            os = new CipherOutputStream(os, ecipher);

// read in the clear text and write to out to encrypt

            int numRead = 0;

            while ((numRead = is.read(buf)) >= 0) {

                os.write(buf, 0, numRead);

            }

// close all streams

            os.close();

        } catch (IOException e) {

            System.out.println("I/O Error:" + e.getMessage());

        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Encryption.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(Encryption.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Encryption.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void decrypt(InputStream is, OutputStream os) {

        try {
            AlgorithmParameterSpec paramSpec = new IvParameterSpec(iv);
            SecretKey key = (SecretKey) readFromFile(filekey);
            dcipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
            byte[] buf = new byte[1024];

// bytes read from stream will be decrypted

            CipherInputStream cis = new CipherInputStream(is, dcipher);

// read in the decrypted bytes and write the clear text to out

            int numRead = 0;

            while ((numRead = cis.read(buf)) >= 0) {

                os.write(buf, 0, numRead);

            }

// close all streams

            cis.close();

            is.close();

            os.close();

        } catch (IOException e) {

            System.out.println("I/O Error:" + e.getMessage());

        } catch (Exception ex) {
            Logger.getLogger(Encryption.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
