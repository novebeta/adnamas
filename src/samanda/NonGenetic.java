package samanda;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JSpinner;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;

import java.awt.Component;
import java.awt.Font;
import java.awt.LayoutManager;

import javax.swing.ButtonGroup;
import javax.swing.SpinnerNumberModel;

import java.awt.event.ActionListener;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.Color;
import pedigree.Utils;

public class NonGenetic extends javax.swing.JPanel {
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	private final ButtonGroup buttonGroup_2 = new ButtonGroup();
	private final ButtonGroup buttonGroup_3 = new ButtonGroup();
	private final ButtonGroup buttonGroup_4 = new ButtonGroup();
	private JSpinner spinner = new JSpinner();
	private JSpinner spinner_1 = new JSpinner();
	private JSpinner spinner_2 = new JSpinner();
	private JSpinner spinner_3 = new JSpinner();
	private JSpinner spinner_4 = new JSpinner();
	private JRadioButton rdbtnTeratur;
	private JRadioButton rdbtnTidakTeratur;
	private JRadioButton rdbtnKbHormonal;
	private JRadioButton rdbtnKbNonHormonal;
	private JRadioButton rdbtnTidakKb;
	private JRadioButton rdbtnYa;
        private JRadioButton rdbtnYa2;
	private JRadioButton rdbtnTidak;
	private JRadioButton radioButton;
	private JRadioButton radioButton_1;
	private JRadioButton radioButton_2;
	private JRadioButton radioButton_3;
	private final JPanel mn = new JPanel();

	/**
	 * Create the panel.
	 */
	public NonGenetic() {
		mn.setLayout(null);
		JLabel lblUmurHaidPertama = new JLabel("Umur Haid Pertama");
		lblUmurHaidPertama.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblUmurHaidPertama.setBounds(10, 13, 115, 23);

		spinner.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0),
				null, new Integer(1)));
		spinner.setFont(new Font("Tahoma", Font.PLAIN, 12));
		spinner.setBounds(183, 11, 44, 23);
                Utils.setSpinnerHandle(spinner);
		Utils.HideArrowSpinner(spinner);

		JLabel lblTahun = new JLabel("Tahun");
		lblTahun.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTahun.setBounds(232, 13, 49, 23);

		JLabel lblJumlahAnak = new JLabel("Jumlah Anak");
		lblJumlahAnak.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblJumlahAnak.setBounds(10, 66, 92, 23);

		spinner_1.setModel(new SpinnerNumberModel(new Integer(0),
				new Integer(0), null, new Integer(1)));
		spinner_1.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				spinner_2.setEnabled(((Number) spinner_1.getValue()).intValue() > 0);
			}
		});

		spinner_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
		spinner_1.setBounds(183, 64, 44, 23);
                Utils.setSpinnerHandle(spinner_1);
		Utils.HideArrowSpinner(spinner_1);

		JLabel lblUsiaMelahirkanPertama = new JLabel("Usia Melahirkan Pertama");
		lblUsiaMelahirkanPertama.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblUsiaMelahirkanPertama.setBounds(10, 93, 163, 23);
		mn.add(lblUmurHaidPertama);
		mn.add(spinner);
		mn.add(lblTahun);
		mn.add(lblJumlahAnak);
		mn.add(spinner_1);
		mn.add(lblUsiaMelahirkanPertama);
		spinner_2.setModel(new SpinnerNumberModel(new Integer(0),
				new Integer(0), null, new Integer(1)));
		spinner_2.setEnabled(false);

		spinner_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		spinner_2.setBounds(183, 91, 44, 23);
                Utils.setSpinnerHandle(spinner_2);
		Utils.HideArrowSpinner(spinner_2);
		mn.add(spinner_2);

		JLabel label = new JLabel("Tahun");
		label.setFont(new Font("Tahoma", Font.PLAIN, 12));
		label.setBounds(232, 93, 54, 23);
		mn.add(label);

		JLabel lblBeratBadan = new JLabel("Berat Badan");
		lblBeratBadan.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblBeratBadan.setBounds(10, 220, 148, 23);
		mn.add(lblBeratBadan);

		spinner_3.setModel(new SpinnerNumberModel(new Double(0), new Double(0),
				null, new Double(1)));
		spinner_3.setFont(new Font("Tahoma", Font.PLAIN, 12));
		spinner_3.setBounds(183, 218, 44, 23);
                Utils.setSpinnerHandle(spinner_3);
		Utils.HideArrowSpinner(spinner_3);
		mn.add(spinner_3);

		JLabel lblTinggiBadan = new JLabel("Tinggi Badan");
		lblTinggiBadan.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTinggiBadan.setBounds(10, 247, 148, 23);
		mn.add(lblTinggiBadan);

		spinner_4.setModel(new SpinnerNumberModel(new Double(0), new Double(0),
				null, new Double(1)));
		spinner_4.setFont(new Font("Tahoma", Font.PLAIN, 12));
		spinner_4.setBounds(183, 245, 44, 23);
                Utils.setSpinnerHandle(spinner_4);
		Utils.HideArrowSpinner(spinner_4);
		mn.add(spinner_4);

		JSeparator separator = new JSeparator();
		separator.setBounds(10, 274, 430, 3);
		mn.add(separator);

		JLabel lblKbHormonal = new JLabel("*: KB Hormonal = Susuk, Pil, Suntik");
		lblKbHormonal.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblKbHormonal.setBounds(10, 284, 375, 23);
		mn.add(lblKbHormonal);

		JLabel lblKg = new JLabel("Kg");
		lblKg.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblKg.setBounds(232, 220, 54, 23);
		mn.add(lblKg);

		JLabel lblCm = new JLabel("Cm");
		lblCm.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCm.setBounds(232, 247, 54, 23);
		mn.add(lblCm);

		JLabel lblSiklusHaid = new JLabel("Siklus Haid");
		lblSiklusHaid.setBounds(10, 36, 99, 23);
		mn.add(lblSiklusHaid);
		lblSiklusHaid.setFont(new Font("Tahoma", Font.PLAIN, 12));

		rdbtnTeratur = new JRadioButton("Teratur");
		rdbtnTeratur.setSelected(true);
		rdbtnTeratur.setBounds(184, 32, 101, 23);
		mn.add(rdbtnTeratur);
		buttonGroup.add(rdbtnTeratur);
		rdbtnTeratur.setFont(new Font("Tahoma", Font.PLAIN, 12));

		rdbtnTidakTeratur = new JRadioButton("Tidak Teratur");
		rdbtnTidakTeratur.setBounds(355, 32, 99, 23);
		mn.add(rdbtnTidakTeratur);
		buttonGroup.add(rdbtnTidakTeratur);
		rdbtnTidakTeratur.setFont(new Font("Tahoma", Font.PLAIN, 12));

		JLabel lblRiwayatKb = new JLabel("Riwayat KB");
		lblRiwayatKb.setBounds(10, 118, 154, 23);
		mn.add(lblRiwayatKb);
		lblRiwayatKb.setFont(new Font("Tahoma", Font.PLAIN, 12));

		rdbtnKbHormonal = new JRadioButton("KB Hormonal*");
		buttonGroup_1.add(rdbtnKbHormonal);
		rdbtnKbHormonal.setBounds(184, 114, 124, 23);
		mn.add(rdbtnKbHormonal);
		rdbtnKbHormonal.setFont(new Font("Tahoma", Font.PLAIN, 12));

		rdbtnKbNonHormonal = new JRadioButton("KB Non Hormonal");
		buttonGroup_1.add(rdbtnKbNonHormonal);
		rdbtnKbNonHormonal.setBounds(355, 114, 135, 23);
		mn.add(rdbtnKbNonHormonal);
		rdbtnKbNonHormonal.setFont(new Font("Tahoma", Font.PLAIN, 12));

		rdbtnTidakKb = new JRadioButton("Tidak KB");
		rdbtnTidakKb.setSelected(true);
		buttonGroup_1.add(rdbtnTidakKb);
		rdbtnTidakKb.setBounds(526, 114, 85, 23);
		mn.add(rdbtnTidakKb);
		rdbtnTidakKb.setFont(new Font("Tahoma", Font.PLAIN, 12));

		JLabel lblMenapouse = new JLabel("Menapause");
		lblMenapouse.setBounds(10, 143, 152, 23);
		mn.add(lblMenapouse);
		lblMenapouse.setFont(new Font("Tahoma", Font.PLAIN, 12));

		rdbtnYa = new JRadioButton("Ya, 55 Thn/sebelumnya");
		buttonGroup_2.add(rdbtnYa);
		rdbtnYa.setBounds(184, 139, 170, 23);
		mn.add(rdbtnYa);
		rdbtnYa.setFont(new Font("Tahoma", Font.PLAIN, 12));

                rdbtnYa2 = new JRadioButton("Ya, 55 Thn keatas");
		buttonGroup_2.add(rdbtnYa2);
		rdbtnYa2.setBounds(355, 139, 170, 23);
		mn.add(rdbtnYa2);
		rdbtnYa2.setFont(new Font("Tahoma", Font.PLAIN, 12));

		rdbtnTidak = new JRadioButton("Tidak");
		rdbtnTidak.setSelected(true);
		buttonGroup_2.add(rdbtnTidak);
		rdbtnTidak.setBounds(526, 139, 99, 23);
		mn.add(rdbtnTidak);
		rdbtnTidak.setFont(new Font("Tahoma", Font.PLAIN, 12));

		JLabel lblTerapiSulihHormon = new JLabel("Terapi Sulih Hormon");
		lblTerapiSulihHormon.setBounds(10, 168, 147, 23);
		mn.add(lblTerapiSulihHormon);
		lblTerapiSulihHormon.setFont(new Font("Tahoma", Font.PLAIN, 12));

		radioButton = new JRadioButton("Ya");
		buttonGroup_3.add(radioButton);
		radioButton.setBounds(184, 164, 110, 23);
		mn.add(radioButton);
		radioButton.setFont(new Font("Tahoma", Font.PLAIN, 12));

		radioButton_1 = new JRadioButton("Tidak");
		radioButton_1.setSelected(true);
		buttonGroup_3.add(radioButton_1);
		radioButton_1.setBounds(355, 164, 99, 23);
		mn.add(radioButton_1);
		radioButton_1.setFont(new Font("Tahoma", Font.PLAIN, 12));

		JLabel lblMerokok = new JLabel("Merokok");
		lblMerokok.setBounds(10, 193, 152, 23);
		mn.add(lblMerokok);
		lblMerokok.setFont(new Font("Tahoma", Font.PLAIN, 12));

		radioButton_2 = new JRadioButton("Ya");
		buttonGroup_4.add(radioButton_2);
		radioButton_2.setBounds(184, 189, 99, 23);
		mn.add(radioButton_2);
		radioButton_2.setFont(new Font("Tahoma", Font.PLAIN, 12));

		radioButton_3 = new JRadioButton("Tidak");
		radioButton_3.setSelected(true);
		buttonGroup_4.add(radioButton_3);
		radioButton_3.setBounds(355, 189, 99, 23);
		mn.add(radioButton_3);
		radioButton_3.setFont(new Font("Tahoma", Font.PLAIN, 12));

		JLabel lblNonGenetics = new JLabel();
		lblNonGenetics.setText("     Non Genetics");
		lblNonGenetics.setOpaque(true);
		lblNonGenetics.setFont(new Font("SansSerif", Font.BOLD, 16));
		lblNonGenetics.setBackground(Color.GRAY);
		lblNonGenetics.setAlignmentX(1.0f);
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(
				Alignment.LEADING).addGroup(
				groupLayout
						.createSequentialGroup()
						.addContainerGap()
						.addGroup(
								groupLayout
										.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNonGenetics,
												Alignment.TRAILING,
												GroupLayout.DEFAULT_SIZE, 640,
												Short.MAX_VALUE)
										.addComponent(mn,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE))
						.addContainerGap()));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(
				Alignment.LEADING).addGroup(
				groupLayout
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(lblNonGenetics,
								GroupLayout.PREFERRED_SIZE, 61,
								GroupLayout.PREFERRED_SIZE)
						.addGap(18)
						.addComponent(mn, GroupLayout.PREFERRED_SIZE, 355,
								GroupLayout.PREFERRED_SIZE)
						.addContainerGap(37, Short.MAX_VALUE)));
		setLayout(groupLayout);
		add(mn);
	}

	public void AddChange(ChangeListener l) {
		spinner.addChangeListener(l);
		spinner_1.addChangeListener(l);
		spinner_2.addChangeListener(l);
		spinner_3.addChangeListener(l);
		spinner_4.addChangeListener(l);
	}

	public void addAction(ActionListener l) {
		radioButton.addActionListener(l);
		radioButton_1.addActionListener(l);
		radioButton_2.addActionListener(l);
		radioButton_3.addActionListener(l);
		rdbtnTeratur.addActionListener(l);
		rdbtnTidakTeratur.addActionListener(l);
		rdbtnKbHormonal.addActionListener(l);
		rdbtnKbNonHormonal.addActionListener(l);
		rdbtnTidakKb.addActionListener(l);
		rdbtnYa.addActionListener(l);
                rdbtnYa2.addActionListener(l);
		rdbtnTidak.addActionListener(l);
	}

	public int getSiklusHaid() {
		return rdbtnTidakTeratur.isSelected() ? 1 : 0;
	}

	public int getRiwayatKB() {
		if (rdbtnKbHormonal.isSelected())
			return 1;
		else if (rdbtnKbNonHormonal.isSelected())
			return 2;
		else
			return 0;
	}

	public int getValMenapause() {
            if(rdbtnYa.isSelected()) {
            return 1;
        }
            if(rdbtnYa2.isSelected()) {
            return  2;
        }
            return 0;
	}

	public int getMerokok() {
		return radioButton_2.isSelected() ? 1 : 0;
	}

	public int getValHaidPertama() {
		return ((Number) spinner.getValue()).intValue();
	}

	public int getHRT() {
		return radioButton.isSelected() ? 1 : 0;
	}

	public int getValJumlahAnak() {
		return ((Number) spinner_1.getValue()).intValue();
	}

	public int getValLahirPertama() {
		return ((Number) spinner_2.getValue()).intValue();
	}

	public double getValBeratBadan() {
		return ((Number) spinner_3.getValue()).doubleValue();
	}

	public double getValTinggiBadan() {
		return ((Number) spinner_4.getValue()).doubleValue();
	}
}
