package samanda;

import com.nexes.wizards.Wizard;
import com.nexes.wizards.WizardPanelDescriptor;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JScrollPane;
import pedigree.Pelican;

public class SamandaPedDesc extends WizardPanelDescriptor {
    public static final String IDENTIFIER = "SAMANDA_PEDIGREE_PANEL";

    Pelican fami;

    public SamandaPedDesc()
    // TODO Auto-generated constructor stub
    {
        getWizard().getDialog().add(new JMenuBar());
        Pelican.parentPane = Wizard.parent;
        fami = new Pelican(); 
        fami.createMenuBar();
        setPanelDescriptorIdentifier(IDENTIFIER);
        setPanelComponent(new JScrollPane(fami));

    }

    @Override
    public Object getNextPanelDescriptor() {
        return SamandaResultDesc.IDENTIFIER;
    }

    public Object getBackPanelDescriptor() {
        return NonGeneticDesc.IDENTIFIER;
    }

    @Override
    public void aboutToHidePanel() {
        //if(fami)
        Wizard.Samanda_file = fami.getFilename();
        Wizard.proband = fami.probandId;
        //return;
        
    }
    
    
    
}
