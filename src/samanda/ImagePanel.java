/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samanda;

import ImagePanel.DraggableImageComponent;
import java.awt.*;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

/**
 *
 * @author axioo
 */
public class ImagePanel extends JPanel {

    Image image = null;
    private Image scaledImage;
    private int imageWidth = 0;
    private int imageHeight = 0;
    //private long paintCount = 0;

    public static JPanel album;
    //constructor
    public ImagePanel() {
        //super();
        //this.image = image;
//        imageWidth = image.getWidth(this);
//        imageHeight = image.getHeight(this);
//        setScaledImage();
        setLayout(new BorderLayout());
       album = new JPanel();
        album.setBackground(Color.darkGray);
        album.setLayout(null);
        add(album, BorderLayout.CENTER);
//        DraggableImageComponent photo = new DraggableImageComponent();
//        add(photo);//Adds this component to main container
//        photo.setImage(image);//Sets image
//        photo.setAutoSize(true);//The component get ratio w/h of source image
//        photo.setOverbearing(true);//On click ,this panel gains lowest z-buffer
//        photo.setBorder(new LineBorder(Color.black, 1));
//
//        //A small randomization of object size/position
//        int delta = getWidth() / 4;
//        int randomGrow = getRandom(getRandom(delta * 2));
//        int cx = getWidth() / 2;
//        int cy = getHeight() / 2;
//        photo.setSize(delta + randomGrow, delta + randomGrow);
//        photo.setLocation(cx + getRandom(delta / 2) - photo.getWidth() / 2, cy + getRandom(delta / 2) - photo.getHeight() / 2);
//        photo.setLocation(0,0);
//        repaint();
    }
    
    public void addNewPhoto(Image img) {
        //Get resources from Directory or Jar file
        //Image img = Toolkit.getDefaultToolkit().createImage("images/"+fileName);
        
        //Creates a draggableImageComponent and adds loaded image
        DraggableImageComponent photo = new DraggableImageComponent();
        album.add(photo);//Adds this component to main container
        photo.setImage(img);//Sets image
        photo.setAutoSize(true);//The component get ratio w/h of source image
        photo.setOverbearing(true);//On click ,this panel gains lowest z-buffer
        photo.setBorder(new LineBorder(Color.black, 1));

        //A small randomization of object size/position
        int delta = album.getWidth() / 4;
        int randomGrow = getRandom(getRandom(delta * 2));
        int cx = album.getWidth() / 2;
        int cy = album.getHeight() / 2;
        photo.setSize(delta + randomGrow, delta + randomGrow);
        photo.setLocation(cx + getRandom(delta / 2) - photo.getWidth() / 2, cy + getRandom(delta / 2) - photo.getHeight() / 2);
        album.repaint();
    }
    
     public static int getRandom(int range) {
        int r = (int) (Math.random() * range) - range;
        return r;
    }

//    public void loadImage(String file) throws IOException {
//        image = ImageIO.read(new File(file));
//        //might be a situation where image isn't fully loaded, and
//        //  should check for that before setting...
//        
//    }

    //e.g., containing frame might call this from formComponentResized
//    public void scaleImage() {
//        setScaledImage();
//    }

    //override paintComponent
//    public void paintComponent(Graphics g) {
//        super.paintComponent(g);
//        if ( scaledImage != null ) {
//            //System.out.println("ImagePanel paintComponent " + ++paintCount);
//            g.drawImage(scaledImage, 0, 0, this);
//        }
//    }
//
//    private void setScaledImage() {
//        if ( image != null ) {
//
//            //use floats so division below won't round
//            float iw = imageWidth;
//            float ih = imageHeight;
//            float pw = this.getWidth();   //panel width
//            float ph = this.getHeight();  //panel height
//
//            if ( pw < iw || ph < ih ) {
//
//                /* compare some ratios and then decide which side of image to anchor to panel
//                   and scale the other side
//                   (this is all based on empirical observations and not at all grounded in theory)*/
//
//                //System.out.println("pw/ph=" + pw/ph + ", iw/ih=" + iw/ih);
//
//                if ( (pw / ph) > (iw / ih) ) {
//                    iw = -1;
//                    ih = ph;
//                } else {
//                    iw = pw;
//                    ih = -1;
//                }
//
//                //prevent errors if panel is 0 wide or high
//                if (iw == 0) {
//                    iw = -1;
//                }
//                if (ih == 0) {
//                    ih = -1;
//                }
//
//                scaledImage = image.getScaledInstance(
//                            new Float(iw).intValue(), new Float(ih).intValue(), Image.SCALE_DEFAULT);
//
//            } else {
//                scaledImage = image;
//            }
//
//            //System.out.println("iw = " + iw + ", ih = " + ih + ", pw = " + pw + ", ph = " + ph);
//        }
//    }

//    public ImagePanel(Image image) {
//        this.image = image;
//    }
//
//    @Override
//    public void paintComponent(Graphics g) {
//        super.paintComponent(g);
//        //there is a picture: draw it
//        if (image != null) {
//            int height = this.getSize().height;
//            int width = this.getSize().width;
//            Graphics2D g2 = (Graphics2D)g;
//            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//            g2.drawImage(image, 0, 0, 480, 480, Color.white,null);
//        }
//    }
}

class ShowCanvas extends JPanel {
        int x, y;
        BufferedImage image;

        ShowCanvas(Image image1) {                
                setBackground(Color.white);
                //setSize(450, 400);
                setPreferredSize(new Dimension(768, 768));
                addMouseMotionListener(new MouseMotionHandler());

                //Image img = getToolkit().getImage("node.jpg");
                Image img = image1;
                MediaTracker mt = new MediaTracker(this);
                mt.addImage(img, 1);
                try {
                        mt.waitForAll();
                } catch (Exception e) {
                        System.out.println("Image not found. "+e.getMessage());
                }
                image = new BufferedImage(img.getWidth(this), img.getHeight(this),
                                BufferedImage.TYPE_INT_ARGB);
                Graphics2D g2 = image.createGraphics();
                g2.drawImage(img, 0, 0, this);
        }

        public void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2D = (Graphics2D) g;
                g2D.drawImage(image, x, y, this);
        }

        class MouseMotionHandler extends MouseMotionAdapter {
                public void mouseDragged(MouseEvent e) {
                        x = e.getX();
                        y = e.getY();
                        repaint();
                }

                public void mouseMoved(MouseEvent e) {
                }
        }
}

class MyCanvas extends JComponent {

    private Image img1;
    public MyCanvas(Image image) {
        super();
        this.img1 = image;
    }

   
    
  public void paint(Graphics g) {
   // Image img1 = Toolkit.getDefaultToolkit().getImage("yourFile.gif");

    int width = img1.getWidth(this);
    int height = img1.getHeight(this);

    int scale = 2;
    int w = scale * width;
    int h = scale * height;
    // explicitly specify width (w) and height (h)
    g.drawImage(img1, 10, 10, (int) w, (int) h, this);

  }
}