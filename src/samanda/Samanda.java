/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package samanda;

import com.nexes.wizards.Wizard;
import com.nexes.wizards.WizardPanelDescriptor;
import com.tomtessier.scrollabledesktop.JScrollableDesktopPane;
import javax.swing.JInternalFrame;
import samanda.properties.LanguagesController;

/**
 *
 * @author axioo
 */
public class Samanda {
    /**
     * @param args the command line arguments
     */
    private JScrollableDesktopPane parent;
    public Wizard wizard;
    public LanguagesController lc;
    Samanda(JScrollableDesktopPane parentPane, String title) {
//            @Override
//            protected void paintComponent(Graphics g) {
//                super.paintComponent(g);
//                BufferedImage s;
//
//                try {
//                    s = ImageIO.read(this.getClass().getResource("bck.jpg"));
//                    // slatetp = new TexturePaint(s, new Rectangle(0, 0, 2, 2));
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    return;
//                }
//                g.drawImage(s, 0, 0, getWidth(), getHeight(), this);
//            }
        try {
//            wizard = new Wizard(parentPane);
            wizard = new Wizard();
            wizard.getDialog().setTitle(title);
            wizard.getDialog().setResizable(true);
//            wizard.getDialog().setClosable(true);
//            wizard.getDialog().setMaximizable(true);
//            wizard.getDialog().setIconifiable(true);
            wizard.getDialog().setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
            //wizard.getDialog().setLocationRelativeTo(wizard.getDialog().getContentPane());
            // wizard.getDialog().setPreferredSize(new Dimension(1024, 768));
            // wizard.getDialog().revalidate();
            //
            WizardPanelDescriptor welcome = null;
            welcome = new WelcomeDesc();
//        try {
//            welcome = new WelcomeDesc();
//        } catch (IOException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
            wizard.registerWizardPanel(WelcomeDesc.IDENTIFIER, welcome);
            WizardPanelDescriptor agree = new AgreementDesc(lc);
            wizard.registerWizardPanel(AgreementDesc.IDENTIFIER, agree);
//            WizardPanelDescriptor clausPed = new ClausPedDesc();
//            wizard.registerWizardPanel(ClausPedDesc.IDENTIFIER, clausPed);
//
//            WizardPanelDescriptor claus = new ClausDesc();
//            wizard.registerWizardPanel(ClausDesc.IDENTIFIER, claus);
            WizardPanelDescriptor ng = new NonGeneticDesc();
            wizard.registerWizardPanel(NonGeneticDesc.IDENTIFIER, ng);
            WizardPanelDescriptor rh = new RelativeHazardDesc();
            wizard.registerWizardPanel(RelativeHazardDesc.IDENTIFIER, rh);
            WizardPanelDescriptor sp = new SamandaPedDesc();
            wizard.registerWizardPanel(SamandaPedDesc.IDENTIFIER, sp);
            WizardPanelDescriptor sr = new SamandaResultDesc(lc);
            wizard.registerWizardPanel(SamandaResultDesc.IDENTIFIER, sr);
            // WizardPanelDescriptor pedClaus = new PedigreeClausDsc();
            // wizard.registerWizardPanel(AgreementDesc.IDENTIFIER, pedClaus);
            wizard.setCurrentPanel(WelcomeDesc.IDENTIFIER);
            //wizard.setModal(false);
//            wizard.getDialog().setLocationRelativeTo(null);
            wizard.getDialog().pack();
            wizard.getDialog().setVisible(true);
//            wizard.getDialog().setSelected(true);
        } catch (Throwable t) {
//            JOptionPane.showMessageDialog(frame, t.toString(),
//            "Pedigree error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
