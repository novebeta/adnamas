/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package samanda.properties;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import pedigree.Utils;
import samanda.MDIApplication;

/**
 *
 * @author LENOVO
 */
public class LanguagesController {
//    private Map supportedLanguages;
    private ResourceBundle translation;
    public LanguagesController(Locale locale) throws Exception {
        try {
            //        Locale INDONESIAN = new Locale.Builder().setLanguage("id").setRegion("US").build();
//        Locale Dutch = new Locale("Dutch", "België", "nl");
            File file = new File(Utils.getJarContainingFolder(MDIApplication.class) + File.separator + "lang");
            URL[] urls = {file.toURI().toURL()};
            System.out.println(file.getPath());
            ClassLoader loader = new URLClassLoader(urls);
            translation = ResourceBundle.getBundle("language", locale, loader);
        } catch (MalformedURLException | URISyntaxException ex) {
            Logger.getLogger(LanguagesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public String getWord(String keyword) {
        return translation.getString(keyword);
    }
}
