/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nexes.wizards;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
/**
 *
 * @author axioo
 */
public class ProgressUtil {
    static class MonitorListener implements ChangeListener, ActionListener {
        ProgressMonitor monitor;
        Window owner;
        Timer timer;
        public MonitorListener(Window owner, ProgressMonitor monitor) {
            this.owner = owner;
            this.monitor = monitor;
        }
        public void stateChanged(ChangeEvent ce) {
            ProgressMonitor monitor = (ProgressMonitor) ce.getSource();
            if (monitor.getCurrent() != monitor.getTotal()) {
                if (timer == null) {
                    timer = new Timer(monitor.getMilliSecondsToWait(), this);
                    timer.setRepeats(false);
                    timer.start();
                }
            } else {
                if (timer != null && timer.isRunning()) {
                    timer.stop();
                }
                monitor.removeChangeListener(this);
            }
        }
        public void actionPerformed(ActionEvent e) {
            monitor.removeChangeListener(this);
            ProgressDialog dlg = owner instanceof Frame
                    ? new ProgressDialog((Frame) owner, monitor)
                    : new ProgressDialog((Dialog) owner, monitor);
            dlg.pack();
            dlg.setLocationRelativeTo(null);
            dlg.setVisible(true);
        }
    }
    public static ProgressMonitor createModalProgressMonitor(Component owner, int total, boolean indeterminate, int milliSecondsToWait) {
        ProgressMonitor monitor = new ProgressMonitor(total, indeterminate, milliSecondsToWait);
        Window window = owner instanceof Window
                ? (Window) owner
                : SwingUtilities.getWindowAncestor(owner);
        monitor.addChangeListener(new MonitorListener(window, monitor));
        return monitor;
    }
}