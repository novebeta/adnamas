/**
 * ******************************************************************
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License as published by the Free
 * Software Foundation; either version 2 of the License, or (at your option) any
 * later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Library General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author Copyright (C) Frank Dudbridge
 *
 *******************************************************************
 */
//package uk.ac.mrc.rfcgr;
package pedigree;

import javax.swing.*;
import java.awt.*;
import java.util.*;

public class PelicanLines extends JPanel {
    private Container panel;
    private boolean showId;
    private boolean showName;
    public static boolean showMarkerNumbers;
    private Vector displayGeno;

    /*
     * {{{ constructor
     */
    public PelicanLines(Container panel, boolean showId, boolean showName,
            boolean showMarkerNumbers, Vector displayGeno) {
        super();
        setBackground(Color.white);
        setOpaque(false);
        setLocation(0, 0);
        this.panel = panel;
        this.showId = showId;
        this.showName = showName;
        this.showMarkerNumbers = showMarkerNumbers;
        this.displayGeno = displayGeno;
        setPreferredSize(panel.getPreferredSize());
    }

    /*
     * }}}
     */

 /*
     * {{{ adjacent
     */
    private boolean adjacent(PelicanPerson father, PelicanPerson mother) {
        if (Math.abs(father.getY() - mother.getY()) >= PelicanPerson.ySize) {
            return (true);
        }
        for (int i = 0; i < panel.getComponentCount(); i++) {
            if (panel.getComponent(i) instanceof PelicanPerson) {
                PelicanPerson person = (PelicanPerson) panel.getComponent(i);
                if (!person.id.equals(father.id)
                        && !person.id.equals(mother.id)
                        && person.getY() >= Math.min(father.getY(),
                                mother.getY())
                        && person.getY() <= Math.max(father.getY(),
                                mother.getY())
                        && person.getX() >= Math.min(father.getX(),
                                mother.getX())
                        && person.getX() <= Math.max(father.getX(),
                                mother.getX())) {
                    return (false);
                }
            }
        }
        return (true);
    }

    /*
     * }}}
     */

 /*
     * {{{ areSibs
     */
    private boolean areSibs(PelicanPerson person1, PelicanPerson person2) {
        if (person1.father == person2.father
                && person1.mother == person2.mother) {
            return (true);
        }
        return (false);
    }

    /*
     * }}}
     */

 /*
     * {{{ paintComponent: draw the lines
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        setPreferredSize(panel.getPreferredSize());
        g.setColor(Color.black);
        int fontHeight = g.getFontMetrics().getHeight();
        int fontAscent = g.getFontMetrics().getAscent();
        int dropSize = Math.max(2, Math.min(PelicanPerson.symbolSize / 2, 3 * (PelicanPerson.ySpace
                - PelicanPerson.symbolSize - fontHeight) / 4));
        for (int i = 0; i < panel.getComponentCount(); i++) {
            if (panel.getComponent(i) instanceof PelicanPerson) {
                // JOptionPane.showMessageDialog(null, );
                // draw a line from this person to its parents
                PelicanPerson person = (PelicanPerson) panel.getComponent(i);
                if (person.father != null && person.mother != null) {
                    // System.out.println("HERE "+String.valueOf(i));
                    // find the mother and father
                    PelicanPerson father = person.father;
                    PelicanPerson mother = person.mother;
                    if (father != null && mother != null) {
                        // line between parents
                        int fatherX = father.getX()
                                + (father.getX() < mother.getX() ? PelicanPerson.symbolSize
                                        : 0);
                        int motherX = mother.getX()
                                + (mother.getX() < father.getX() ? PelicanPerson.symbolSize
                                        : 0);
                        int fatherY = father.getY() + PelicanPerson.symbolSize
                                / 2;
                        int motherY = mother.getY() + PelicanPerson.symbolSize
                                / 2;
                        int leftX = fatherX;
                        int leftY = fatherY;
                        int rightX = motherX;
                        int rightY = motherY;
                        if (motherX < fatherX) {
                            leftX = motherX;
                            leftY = motherY;
                            rightX = fatherX;
                            rightY = fatherY;
                            // JOptionPane.showMessageDialog(null, "");
                        }
                        int gap = PelicanPerson.xSpace
                                - PelicanPerson.symbolSize;
                        // see if any subjects lie between the father and mother
                        if (!adjacent(father, mother)
                                && father.generation == mother.generation) {
//                             draw lines which avoid other symbols
                            g.drawLine(leftX, leftY, leftX + gap / 4, leftY);
                            g.drawLine(rightX, rightY, rightX - gap / 2, rightY);
                            leftX += gap / 4;
                            rightX -= gap / 2;
                            g.drawLine(leftX, leftY, leftX, leftY
                                    - (PelicanPerson.symbolSize + dropSize) / 2);
                            g.drawLine(rightX, rightY, rightX, rightY
                                    - (PelicanPerson.symbolSize + dropSize) / 2);
                            leftY -= (PelicanPerson.symbolSize + dropSize) / 2;
                            rightY -= (PelicanPerson.symbolSize + dropSize) / 2;
                        }
                        //ini yang garis horiz antar parent
                        g.drawLine(leftX, leftY, rightX, rightY);
                        // line up from child
                        g.drawLine(
                                person.getX() + PelicanPerson.symbolSize / 2,
                                person.getY(), person.getX()
                                + PelicanPerson.symbolSize / 2,
                                person.getY() - dropSize);
                        // line across from child
                        // try to attach to an orphan parent
                        int parentX = fatherX;
                        if (father.isOrphan() || mother.isOrphan()) {
                            parentX = Math.max(fatherX, motherX) - gap / 2;
                        } else {
                            // if no orphan parents, go straight up from
                            // middle laid out sib
                            int nsib = 0;
                            for (int j = 0; j < panel.getComponentCount(); j++) {
                                if (panel.getComponent(j) instanceof PelicanPerson) {
                                    PelicanPerson sib = (PelicanPerson) panel.getComponent(j);
                                    if (areSibs(person, sib)) {
                                        nsib++;
                                    }
                                }
                            }
                            int sibs = 0;
                            for (int j = 0; j < panel.getComponentCount()
                                    && sibs <= nsib / 2; j++) {
                                if (panel.getComponent(j) instanceof PelicanPerson) {
                                    PelicanPerson sib = (PelicanPerson) panel.getComponent(j);
                                    if (areSibs(person, sib)) {
                                        sibs++;
                                    }
                                    parentX = sib.getX()
                                            + PelicanPerson.symbolSize / 2;
                                }
                            }
                            if (nsib > 1 && nsib % 2 == 0) {
                                parentX -= PelicanPerson.xSpace / 2;
                            }
                            if (parentX <= leftX) {
                                parentX = leftX + PelicanPerson.symbolSize / 2;
                            }
                            if (parentX >= rightX) {
                                parentX = rightX - PelicanPerson.symbolSize / 2;
                            }
                        }
                        g.drawLine(
                                person.getX() + PelicanPerson.symbolSize / 2,
                                person.getY() - dropSize, parentX,
                                person.getY() - dropSize);
                        // line up to parents
                        // Draw a vertical line up to the line joining the
                        // parents
                        // if this happens to be not between the parents,
                        // change it to a line to the midpoint between the
                        // parents
                        int parentY = (rightX != leftX) ? leftY
                                + (rightY - leftY) * (parentX - leftX)
                                / (rightX - leftX) : (leftY + rightY) / 2;
                        if (rightX == leftX
                                || parentY > Math.max(leftY, rightY)
                                || parentY < Math.min(leftY, rightY)) {
                            g.drawLine(parentX, person.getY() - dropSize,
                                    (leftX + rightX) / 2, (leftY + rightY) / 2);
                        } else {
                            //ini untuk gambar garis vertical
                            g.drawLine(parentX, person.getY() - dropSize,
                                    parentX, parentY);
                        }
                    }
                }
                if (!person.spounse.isEmpty()) {
                    PelicanPerson spounse = (PelicanPerson) person.spounse.firstElement();
                    //System.out.println(person.id + " " + spounse.id);
                    int fatherX = person.getX()
                            + (person.getX() < spounse.getX() ? PelicanPerson.symbolSize
                                    : 0);
                    int motherX = spounse.getX()
                            + (spounse.getX() < person.getX() ? PelicanPerson.symbolSize
                                    : 0);
                    int fatherY = person.getY() + PelicanPerson.symbolSize
                            / 2;
                    int motherY = spounse.getY() + PelicanPerson.symbolSize
                            / 2;
                    int leftX = fatherX;
                    int leftY = fatherY;
                    int rightX = motherX;
                    int rightY = motherY;
                    if (motherX < fatherX) {
                        leftX = motherX;
                        leftY = motherY;
                        rightX = fatherX;
                        rightY = fatherY;
                        // JOptionPane.showMessageDialog(null, "");
                    }
                    int gap = PelicanPerson.xSpace
                            - PelicanPerson.symbolSize;
                    // see if any subjects lie between the father and mother
                    if (!adjacent(person, spounse)
                            && person.generation == spounse.generation) {
//                             draw lines which avoid other symbols
                        g.drawLine(leftX, leftY, leftX + gap / 4, leftY);
                        g.drawLine(rightX, rightY, rightX - gap / 2, rightY);
                        leftX += gap / 4;
                        rightX -= gap / 2;
                        g.drawLine(leftX, leftY, leftX, leftY
                                - (PelicanPerson.symbolSize + dropSize) / 2);
                        g.drawLine(rightX, rightY, rightX, rightY
                                - (PelicanPerson.symbolSize + dropSize) / 2);
                        leftY -= (PelicanPerson.symbolSize + dropSize) / 2;
                        rightY -= (PelicanPerson.symbolSize + dropSize) / 2;
                    }
                    //ini yang garis horiz antar parent
                    g.drawLine(leftX, leftY, rightX, rightY);
                }
                // write out age
                int verticalPosn = person.getY() + PelicanPerson.symbolSize
                        + fontAscent;
                if (showId) {
                    int fontWidth = g.getFontMetrics().stringWidth(person.id);
                    g.drawString(person.id, person.getX()
                            + PelicanPerson.symbolSize / 2 - fontWidth / 2,
                            verticalPosn);
                    if (person.dna == PelicanPerson.with_dna) {
                        g.drawString("+", person.getX()
                                + PelicanPerson.symbolSize / 2 + fontWidth / 2,
                                verticalPosn);
                    }
                    verticalPosn += fontAscent;
                }
                // write out name
                if (showMarkerNumbers) {
                    int fontWidth = g.getFontMetrics().stringWidth(person.id);
                    g.drawString(person.id, person.getX()
                            + PelicanPerson.symbolSize / 2 - fontWidth / 2,
                            verticalPosn);
                    // g.drawString(person.id,(PelicanPerson.symbolSize-g.getFontMetrics().stringWidth(person.id))/2,(PelicanPerson.symbolSize+g.getFontMetrics().getAscent())/2);
                    verticalPosn += fontAscent;
                }
                if (showName) {
                    int fontWidth = g.getFontMetrics().stringWidth(person.name);
                    g.drawString(person.name, person.getX()
                            + PelicanPerson.symbolSize / 2 - fontWidth / 2,
                            verticalPosn);
                    verticalPosn += fontAscent;
                }
                // mark dead subjects
                if (!showMarkerNumbers) {
                    String labelAge = "c." + person.age;
                    if (person.dead) {
                        int deathRight = (int) (PelicanPerson.symbolSize * 1.2);
                        int deathLeft = (int) (PelicanPerson.symbolSize * 0.2);
                        g.drawLine(person.getX() + deathRight, person.getY()
                                - deathLeft, person.getX() - deathLeft,
                                person.getY() + deathRight);
                        labelAge = "d." + person.age;
                    }
                    int fontWidthAge = g.getFontMetrics().stringWidth(labelAge);
                    if (person.ageProx == 1 || person.ageDeadProx == 1) {
                        g.setColor(Color.BLUE);
                    }
                    g.drawString(labelAge, person.getX()
                            + PelicanPerson.symbolSize / 2 - fontWidthAge / 2,
                            verticalPosn);
                    g.setColor(Color.BLACK);
                    verticalPosn += fontAscent;
                    String labelString = "";
                    int myX = 0;
                    int pnjg = 0;
                    if (person.isAffectedBreast != 0) {
//                        String label = "BC:";
                        if (person.bilateralitas == 0) {
                            labelString = "BCbil:" + person.affectedBreast + "/" + person.age_bilateral;
                        } else {
                            labelString = "BC:" + person.affectedBreast;
                        }
                        //labelString = ("BC:" + person.affectedBreast);
                        int fontWidthBreast = g.getFontMetrics().stringWidth(
                                labelString);
                        if (person.BreastProx == 1) {
                            g.setColor(Color.BLUE);
                        }
                        myX = person.getX() + PelicanPerson.symbolSize / 2 - fontWidthBreast / 2;
                        if (person.isAffectedOvary != 0) {
                            myX = person.getX() + PelicanPerson.symbolSize / 2 - fontWidthBreast;
                        }
                        g.drawString(labelString, myX, verticalPosn);
                        g.setColor(Color.BLACK);
                    }
                    if (person.isAffectedOvary != 0) {
                        labelString = (" OC:" + person.affectedOvary);
                        int fontWidthAffected = g.getFontMetrics().stringWidth(
                                labelString);
                        if (person.OvaryProx == 1) {
                            g.setColor(Color.BLUE);
                        }
//                        g.drawString(labelString, person.getX()
//                            + PelicanPerson.symbolSize / 2 - fontWidthAffected
//                            / 2, verticalPosn);
                        if (myX == 0) {
                            myX = person.getX() + PelicanPerson.symbolSize / 2 - fontWidthAffected / 2;
                        } else {
                            myX = person.getX() + PelicanPerson.symbolSize / 2;
                        }
                        g.drawString(labelString, myX, verticalPosn);
                        g.setColor(Color.BLACK);
                    }                    
                    if (person.otherCancer != 0) {
                        String label = "";
                        if (person.descOtherCancer.length() >= 5) {
                            label = person.descOtherCancer.substring(0, 5);
                        }
                        labelString = (" " + label);
                        int fontWidthAffected = g.getFontMetrics().stringWidth(
                                labelString);
//                        if (person.OvaryProx == 1) {
//                            g.setColor(Color.BLUE);
//                        }
//                        g.drawString(labelString, person.getX()
//                            + PelicanPerson.symbolSize / 2 - fontWidthAffected
//                            / 2, verticalPosn);
                        if (myX == 0) {
                            myX = person.getX() + PelicanPerson.symbolSize / 2 - fontWidthAffected / 2;
                        } else {
                            myX = person.getX() + PelicanPerson.symbolSize / 2;
                        }
                        g.drawString(labelString, myX, verticalPosn);
                        g.setColor(Color.BLACK);
                    }
                    verticalPosn += fontAscent;
                }
                // }
                if (showMarkerNumbers) {
                    int maxWidth = 0;
                    for (int j = 0; j < person.genotype.size(); j++) {
                        //int index = ((Integer) displayGeno.get(j)).intValue();
                        Vector geno = (Vector) person.genotype.get(j);
                        int fontWidth = g.getFontMetrics().stringWidth(
                                String.valueOf(geno.get(0)))
                                + g.getFontMetrics().stringWidth(" ") / 2;
                        maxWidth = Math.max(maxWidth, fontWidth);
                        g.drawString(String.valueOf(geno.get(0)) + " "
                                + String.valueOf(geno.get(1)),
                                person.getX() + PelicanPerson.symbolSize / 2
                                - fontWidth, verticalPosn + j
                                * fontAscent);
                        // System.out.println(displayGeno.size() + " " + index + " " + j);
                    }
                }
                // write out genotypes
//                if (showMarkerNumbers) {
//                    int maxWidth = 0;
//                    for (int j = 0; j < displayGeno.size(); j++) {
//                        int index = ((Integer) displayGeno.get(j)).intValue();
//                        Vector geno = (Vector) person.genotype.get(index - 1);
//                        int fontWidth = g.getFontMetrics().stringWidth(
//                                String.valueOf(geno.get(0)))
//                                + g.getFontMetrics().stringWidth(" ") / 2;
//                        maxWidth = Math.max(maxWidth, fontWidth);
//                        g.drawString(String.valueOf(geno.get(0)) + " "
//                                + String.valueOf(geno.get(1)),
//                                person.getX() + PelicanPerson.symbolSize / 2
//                                - fontWidth, verticalPosn + j
//                                * fontAscent);
//                    }
//                }
                // for(int j=0;j<displayGeno.size();j++) {
                // int
                // fontWidth=g.getFontMetrics().stringWidth(displayGeno.get(j)+" ")+maxWidth;
                // g.drawString(displayGeno.get(j).toString(),person.getX()+PelicanPerson.symbolSize/2-fontWidth,verticalPosn+j*fontAscent);
                // }
                // proband arrow
                if (person.proband) {
                    int scale = PelicanPerson.symbolSize;
                    int pointX = person.getX() + scale / 8;
                    int pointY = person.getY() + 9 * scale / 8;
                    double sinTheta = 0.25881905; // 15 degree angle
                    double cosTheta = 0.96592583;
                    int[] arrowX = {pointX,
                        pointX - (int) (scale / 3 * cosTheta),
                        pointX - scale / 6, pointX - scale / 3,
                        pointX - scale / 3 + 1, pointX - scale / 6 + 1,
                        pointX - (int) (scale / 3 * sinTheta) + 1};
                    int[] arrowY = {pointY,
                        (int) (pointY + scale / 3 * sinTheta),
                        pointY + scale / 6, pointY + scale / 3,
                        pointY + scale / 3 + 1, pointY + scale / 6 + 1,
                        (int) (pointY + scale / 3 * cosTheta) + 1};
                    Polygon arrow = new Polygon(arrowX, arrowY, 7);
                    g.drawPolygon(arrow);
                    g.fillPolygon(arrow);
                }
            }
        }
    }

    /*
     * }}}
     */
}
