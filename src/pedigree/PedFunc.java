/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pedigree;

/**
 *
 * @author axioo
 */
public class PedFunc {

    public static void addAtrribute(PelicanPerson pp, String name, int age, int ap,boolean ab, 
            boolean ao, int ageB, int ageBp,int ageO,int ageOp) {
        pp.name = name;
        pp.age = age;
        pp.isAffectedBreast = ab ? 1 : 0;
        pp.isAffectedOvary = ao ? 1 : 0;
        pp.affectedBreast = ab ? ageB : age;
        pp.affectedOvary = ao ? ageO : age;
        pp.ageProx = ap;
        pp.BreastProx = ageBp;
        pp.OvaryProx = ageOp;
    }

    public static boolean areParent(PelicanPerson person1, PelicanPerson person2) {
        if (person1 == null
                || person2 == null) {
            return false;
        }

        if (person1.father == null
                && person1.mother == null) {
            return false;
        }

        if (person1.father == person2
                || person1.mother == person2) {
            return (true);
        }
        return (false);
    }

    public static boolean areChild(PelicanPerson person1, PelicanPerson person2) {
        return PedFunc.areParent(person2, person1);
    }

    public static boolean isChild(PelicanPerson child, PelicanPerson person1,
            PelicanPerson person2) {
        if (child.father == person1 && child.mother == person2
                || child.mother == person1 && child.father == person2) {
            return (true);
        }
        return (false);
    }

    public static boolean areSibs(PelicanPerson person1, PelicanPerson person2) {
        if (person1.father == null
                && person1.mother == null) {
            return false;
        }

        if (person2.father == null
                && person2.mother == null) {
            return false;
        }

        if (person1.father == person2.father
                || person1.mother == person2.mother) {
            return (true);
        }
        return (false);
    }

    public static boolean areUncleOrAunt(PelicanPerson person1, PelicanPerson person2) {
        if (person1 == null || person2 == null) {
            return false;
        }

        if (person1.father == null
                && person1.mother == null) {
            return false;
        }

        if (areParent(person1, person2)) {
            return false;
        }

        if (areSibs(person1.father, person2)
                || areSibs(person1.mother, person2)) {
            return true;
        }

        return false;
    }

    public static boolean areUncleOrAuntMateral(PelicanPerson person1, PelicanPerson person2) {
        if (person1 == null || person2 == null) {
            return false;
        }

        if (person1.father == null
                && person1.mother == null) {
            return false;
        }

        if (!person1.mother.hasFather()
                || !person1.mother.hasMother()) {
            return false;
        }

        if (PedFunc.areParent(person1, person2)) {
            return false;
        }

        if (areSibs(person1.father, person2)
                || areSibs(person1.mother, person2)) {
            return true;
        }

        return false;
    }

    public static boolean areUncleOrAuntPateral(PelicanPerson person1, PelicanPerson person2) {
        if (person1 == null || person2 == null) {
            return false;
        }

        if (person1.father == null
                && person1.mother == null) {
            return false;
        }

        if (!person1.father.hasFather()
                || !person1.father.hasMother()) {
            return false;
        }

        if (PedFunc.areParent(person1, person2)) {
            return false;
        }

        if (areSibs(person1.father, person2)
                || areSibs(person1.mother, person2)) {
            return true;
        }

        return false;
    }

    public static boolean areNephewOrNiece(PelicanPerson person1, PelicanPerson person2) {
        return areUncleOrAunt(person2, person1);
    }

    public static boolean areGrandParentMaternal(PelicanPerson person1, PelicanPerson person2) {
        if (!person1.hasMother()) {
            return false;
        }

        if (areParent(person1.mother, person2)) {
            return true;
        }

        return false;
    }

    public static boolean areGrandParentPaternal(PelicanPerson person1, PelicanPerson person2) {
        if (!person1.hasFather()) {
            return false;
        }

        if (areParent(person1.father, person2)) {
            return true;
        }

        return false;
    }
    
    public static boolean areGrandParent(PelicanPerson person1, PelicanPerson person2) {
        if (!person1.hasFather()) {
            return false;
        }
        if (!person1.hasMother()) {
            return false;
        }
        if (areParent(person1.father, person2)) {
            return true;
        }
       
        if (areParent(person1.mother, person2)) {
            return true;
        }
        return false;
    }

    public static boolean areGrandChild(PelicanPerson person1, PelicanPerson person2) {
        return areGrandParent(person2, person1);
    }

    public static boolean areGreatGrantParentMaternal(PelicanPerson person1, PelicanPerson person2) {
        if (!person1.hasMother()) {
            return false;
        }

//        if(!person1.father.hasFather() || !person1.father.hasMother() ||
//                !person1.mother.hasFather() || !person1.mother.hasMother())
//        {
//            return false;
//        }

        if (areGrandParentMaternal(person1.mother, person2) || areGrandParentPaternal(person1.mother, person2)) {
            return true;
        }

        return false;
    }

    public static boolean areGreatGrantParentPaternal(PelicanPerson person1, PelicanPerson person2) {
        if (!person1.hasFather()) {
            return false;
        }

//        if(!person1.father.hasFather() || !person1.father.hasMother() ||
//                !person1.mother.hasFather() || !person1.mother.hasMother())
//        {
//            return false;
//        }
        if (areGrandParentMaternal(person1.father, person2) || areGrandParentPaternal(person1.father, person2)) {
            return true;
        }

        return false;
    }

    public static boolean areCousinMaternal(PelicanPerson person1, PelicanPerson person2) {
        boolean result = false;

        if (!person1.hasMother()) {
            return false;
        }

        if (person2.hasFather()) {
            result = areUncleOrAuntMateral(person1, person2.father);
        }

        if (person2.hasMother()) {
            result = areUncleOrAuntMateral(person1, person2.mother) || result;
        }

        return result;
    }

    public static boolean areCousinPaternal(PelicanPerson person1, PelicanPerson person2) {
        boolean result = false;
        if (!person1.hasFather()) {
            return false;
        }

        if (person2.hasFather()) {
            result = areUncleOrAuntPateral(person1, person2.father);
        }

        if (person2.hasMother()) {
            result = areUncleOrAuntPateral(person1, person2.mother) || result;
        }

        return result;
    }

    public static boolean areGreatGrandson(PelicanPerson person1, PelicanPerson person2) {
        return areGreatGrantParentMaternal(person2, person1) || areGreatGrantParentPaternal(person2, person1);
    }

    public static boolean isFirstDegree(PelicanPerson pp, PelicanPerson person) {
        if (pp == null || person == null) {
            return false;
        }

        if (person == pp) {
            return false;
        }

        if (areParent(pp, person)) {
            return true;

        }

        if (areSibs(pp, person)) {
            return true;
        }

        if (areChild(pp, person)) {
            return true;
        }
        return false;
    }

    public static boolean isThirdDegree(PelicanPerson pp, PelicanPerson person) {
        if (pp == null || person == null) {
            return false;
        }

        if (person == pp) {
            return false;
        }

        if (areGreatGrandson(pp, person)) {
            return true;
        }
        return false;
    }

    public static boolean isThirdDegreeMaternal(PelicanPerson pp, PelicanPerson person) {
        if (pp == null || person == null) {
            return false;
        }

        if (person == pp) {
            return false;
        }

        if (PedFunc.areCousinMaternal(pp, person)) {
            return true;
        }

        if (PedFunc.areGreatGrantParentMaternal(pp, person)) {
            return true;
        }

        return false;
    }

    public static boolean isThirdDegreePaternal(PelicanPerson pp, PelicanPerson person) {
        if (pp == null || person == null) {
            return false;
        }

        if (person == pp) {
            return false;
        }
        if (areCousinPaternal(pp, person)) {
            return true;
        }

        if (areGreatGrantParentPaternal(pp, person)) {
            return true;
        }
        return false;
    }

    public static boolean isSecondDegree(PelicanPerson pp, PelicanPerson person) {
        if (pp == null || person == null) {
            return false;
        }

        if (person == pp) {
            return false;
        }

        if (areNephewOrNiece(pp, person)) {
            return true;
        }

        if (areGrandChild(pp, person)) {
            return true;
        }
        return false;
    }

    public static boolean isSecondDegreeMaternal(PelicanPerson pp, PelicanPerson person) {
        if (pp == null || person == null) {
            return false;
        }

        if (person == pp) {
            return false;
        }

        if (areUncleOrAuntMateral(pp, person)) {
            return true;
        }

        if (areGrandParentMaternal(pp, person)) {
            return true;
        }
        return false;
    }

    public static boolean isSecondDegreePaternal(PelicanPerson pp, PelicanPerson person) {
        if (pp == null || person == null) {
            return false;
        }

        if (person == pp) {
            return false;
        }
        if (areUncleOrAuntPateral(pp, person)) {
            return true;
        }

        if (areGrandParentPaternal(pp, person)) {
            return true;
        }
        return false;
    }
}
