/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableInfo;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author axioo
 */
public class CustomersDao extends BaseDaoImpl<Customers, Integer> {

    private ListPedDao ListPedDao;
    
    public CustomersDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, Customers.class);
        TableUtils.createTableIfNotExists(connectionSource, Customers.class);
        TableInfo ti;
        ti = new TableInfo(connectionSource, this, Customers.class);
        Dao<Customers, String> dao
                = DaoManager.createDao(connectionSource, Customers.class);
        if (!this.hasColumn("AcronymName")) {
            dao.executeRaw("ALTER TABLE `customer` ADD COLUMN AcronymName VARCHAR;");
        }
    }

    public void performDBOperations(ConnectionSource connectionSource)
            throws SQLException {
    }
    
    public boolean hasColumn(String columnName) {
        GenericRawResults<String[]> results;
        try {
            results = this.queryRaw("SELECT " + columnName + " FROM customer");
        } catch (SQLException ex) {
            return false;
        }
        return true;
    }
    
    @Override
    public int delete(Customers cust) {
        // first delete the clients that match the city's id
        //       DeleteBuilder db = pedigreeDao.deleteBuilder();
        try {
//        db.where().eq("id_listped", listPed.id);
//        pedigreeDao.delete(db.prepare());
            QueryBuilder<ListPed, Integer> db = ListPedDao.queryBuilder();
            db.where().eq("Id_Customers", cust.Id_Customers);
            List<ListPed> results = ListPedDao.query(db.prepare());
            for (ListPed en : results) {
                ListPedDao.delete(en);
            }
            // then call the super to delete the city
            return super.delete(cust);
        } catch (SQLException ex) {
            Logger.getLogger(ListPedDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
}
