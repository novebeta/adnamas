/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author axioo
 */
public class ListPedDao extends BaseDaoImpl<ListPed, Integer> {

    private PersonDao pedigreeDao;

    public ListPedDao(ConnectionSource cs, PersonDao clientDao) throws SQLException {
        super(cs, ListPed.class);
        TableUtils.createTableIfNotExists(connectionSource, ListPed.class);
        this.pedigreeDao = clientDao;
    }

    public void performDBOperations(ConnectionSource connectionSource)
            throws SQLException {
    }

    @Override
    public int delete(ListPed listPed) {
        // first delete the clients that match the city's id
        //       DeleteBuilder db = pedigreeDao.deleteBuilder();
        try {
//        db.where().eq("id_listped", listPed.id);
//        pedigreeDao.delete(db.prepare());
            QueryBuilder<Person, Integer> db = pedigreeDao.queryBuilder();
            db.where().eq("id_listped", listPed.id);
            List<Person> results = pedigreeDao.query(db.prepare());
            for (Person en : results) {
                pedigreeDao.delete(en);
            }
            // then call the super to delete the city
            return super.delete(listPed);
        } catch (SQLException ex) {
            Logger.getLogger(ListPedDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
}
