/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableInfo;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author axioo
 */
public class PersonDao extends BaseDaoImpl<Person, Integer> {

    private Dao<Genotype, Integer> genotypeDao;
    private Dao<Spounse, Integer> spounsesDao;

    public PersonDao(ConnectionSource cs, Dao<Genotype, Integer> genotype, Dao<Spounse, Integer> spounses) throws SQLException {
        super(cs, Person.class);
        TableUtils.createTableIfNotExists(connectionSource, Person.class);
        TableInfo ti;
        ti = new TableInfo(cs, this, Person.class);
        Dao<Person, String> dao
                = DaoManager.createDao(cs, Person.class);
        if (!this.hasColumn("brca1")) {
            dao.executeRaw("ALTER TABLE `person` ADD COLUMN brca1 INTEGER;");
        }
        if (!this.hasColumn("brca2")) {
            dao.executeRaw("ALTER TABLE `person` ADD COLUMN brca2 INTEGER;");
        }
        if (!this.hasColumn("p53")) {
            dao.executeRaw("ALTER TABLE `person` ADD COLUMN p53 INTEGER;");
        }
        if (!this.hasColumn("er")) {
            dao.executeRaw("ALTER TABLE `person` ADD COLUMN er INTEGER;");
        }
        if (!this.hasColumn("pr")) {
            dao.executeRaw("ALTER TABLE `person` ADD COLUMN pr INTEGER;");
        }
        if (!this.hasColumn("her2")) {
            dao.executeRaw("ALTER TABLE `person` ADD COLUMN her2 INTEGER;");
        }
        if (!this.hasColumn("ck56")) {
            dao.executeRaw("ALTER TABLE `person` ADD COLUMN ck56 INTEGER;");
        }
        if (!this.hasColumn("ck14")) {
            dao.executeRaw("ALTER TABLE `person` ADD COLUMN ck14 INTEGER;");
        }
        if (!this.hasColumn("Birth_Date")) {
            dao.executeRaw("ALTER TABLE `person` ADD COLUMN Birth_Date INTEGER;");
        }
        if (!this.hasColumn("hrtYears")) {
            dao.executeRaw("ALTER TABLE `person` ADD COLUMN hrtYears INTEGER;");
        }
        this.genotypeDao = genotype;
        this.spounsesDao = spounses;
    }

    public void performDBOperations(ConnectionSource connectionSource)
            throws SQLException {
    }

    @Override
    public int delete(Person pedigree) {
        try {
            // first delete the clients that match the city's id
            DeleteBuilder db = genotypeDao.deleteBuilder();
            db.where().eq("id_person", pedigree.id);
            genotypeDao.delete(db.prepare());
            DeleteBuilder dbS = spounsesDao.deleteBuilder();
            dbS.where().eq("id_person", pedigree.id).and().eq("id_listped", pedigree.listPed.id);
            spounsesDao.delete(dbS.prepare());
            // then call the super to delete the city
            return super.delete(pedigree);
        } catch (SQLException ex) {
            Logger.getLogger(PersonDao.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    public boolean hasColumn(String columnName) {
        GenericRawResults<String[]> results;
        try {
            results = this.queryRaw("SELECT " + columnName + " FROM person");
        } catch (SQLException ex) {
            return false;
        }
        return true;
    }
}
