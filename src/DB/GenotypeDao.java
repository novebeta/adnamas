/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import java.sql.SQLException;

/**
 *
 * @author axioo
 */
public class GenotypeDao extends BaseDaoImpl<Genotype, Integer> {

    public GenotypeDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, Genotype.class);
        TableUtils.createTableIfNotExists(connectionSource, Genotype.class);
    }

    public void performDBOperations(ConnectionSource connectionSource)
            throws SQLException {
    }
}
