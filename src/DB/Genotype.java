/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 *
 * @author axioo
 */
@DatabaseTable(tableName = "genotype")
public class Genotype {
    @DatabaseField(generatedId = true)
    public int id;
    @DatabaseField
    public int pos;
    @DatabaseField
    public int first;
    @DatabaseField
    public int second;
    
    @DatabaseField(foreign = true, columnName = "id_person")
    public Person pedigree;
    
    public Genotype(){
        
    }
    
    public Genotype(int pos,int first,int second,Person ped){
        this.pos = pos;
        this.first = first;
        this.second = second;
        this.pedigree = ped;
    }
}
