/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.util.Date;

/**
 *
 * @author axioo
 */
@DatabaseTable(tableName = "Customers")
public class Customers {
    public static String ID_CUSTOMERS = "Id_Customers";
    @DatabaseField(id = true)
    public String Id_Customers;
    @DatabaseField
    public String No_Rekam_Medik;
    @DatabaseField(unique = true)
    public String Name;
    @DatabaseField
    public String AcronymName;
    @DatabaseField
    public String Home_Phone;
    @DatabaseField
    public String Mobile_Phone;
    @DatabaseField
    public String Address;
    @DatabaseField
    public String City;
    @DatabaseField
    public String Province;
    @DatabaseField
    public String Zip_Postal;
    @DatabaseField
    public String Country;
    @DatabaseField
    public String Notes;
    @DatabaseField
    public String Create_Date;
    @DatabaseField
    public Date Birth_Date;
    @DatabaseField
    public String Directory;
    @DatabaseField
    public String ID_Number;
    @DatabaseField
    public String Etnik;
    @DatabaseField
    public String EtnikOther;
    public Customers() {
        this.Id_Customers = java.util.UUID.randomUUID().toString();
    }
}
